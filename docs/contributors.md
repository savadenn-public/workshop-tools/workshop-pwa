# Authors and contributors {#contributors}

Time Manager is made by [Savadenn](https://www.savadenn.bzh) with the help of individual contributors.

The complete list can be found [here](https://gitlab.com/savadenn-public/workshop-tools/workshop-pwa/-/graphs/latest).
