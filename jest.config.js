module.exports = {
  resetMocks: true,
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '^.+\\.tsx?$': 'ts-jest',
  },
  testMatch: ['**/__tests__/**/?(*.)+(spec|test).[jt]s?(x)'],
  collectCoverageFrom: [
    'src/**/*.{js,ts,vue}',
    '!src/types/**',
    '!src/**/__tests__/**',
  ],
  setupFiles: ['./src/__tests__/setup.ts'],
  reporters: [
    'jest-junit',
    [
      'jest-html-reporters',
      { publicPath: './coverage', filename: 'tests.html' },
    ],
  ],
  coverageReporters: ['lcov', 'cobertura', 'text-summary'],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
  },
  testEnvironment: 'jsdom',
}
