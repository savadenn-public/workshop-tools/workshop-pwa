import axios, {
  AxiosError,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosRequestHeaders,
} from 'axios'
import { getToken, refreshTokens } from '@/api/security'
import { settings } from '@/states'

const apiClientPromise: {
  secure: Promise<AxiosInstance> | null
  public: Promise<AxiosInstance> | null
} = {
  secure: null,
  public: null,
}

/**
 * Add headers to request
 */
function addHeaders(config: AxiosRequestConfig): void {
  if (!config.headers) config.headers = {}

  config.headers['Accept-Language'] = settings.lang

  /* istanbul ignore next */
  if (!config.method) return

  if (config.method.toLocaleLowerCase() === 'post') {
    config.headers['Content-Type'] = 'application/json'
  } else if (config.method.toLocaleLowerCase() === 'patch') {
    config.headers['Content-Type'] = 'application/merge-patch+json'
  }
}

/**
 * Creates GraphQL Client
 */
export async function createApiClient(
  secured: boolean,
  axiosInstance?: AxiosInstance
): Promise<AxiosInstance> {
  const client = axiosInstance ?? axios.create({ baseURL: '/' })

  // Inject JWT_TOKEN if present
  client.interceptors.request.use(async (config) => {
    addHeaders(config)

    if (secured) {
      const token = await getToken()
      if (token) {
        ;(
          config.headers as AxiosRequestHeaders
        ).Authorization = `Bearer ${token}`
      } else {
        return Promise.reject('No bearer token, aborting api call')
      }
    }
    return config
  })

  client.interceptors.response.use(
    (response) => response,
    async (error: AxiosError) => {
      if (
        secured &&
        error?.response?.status === 401 &&
        (await refreshTokens())
      ) {
        return client.request(error.config)
      }
      return Promise.reject(error)
    }
  )

  return client
}

/**
 * Returns secure API client
 */
export async function getApiClient(): Promise<AxiosInstance> {
  if (!apiClientPromise.secure) {
    apiClientPromise.secure = createApiClient(true)
  }
  return apiClientPromise.secure
}

/**
 * Returns API client for unauthenticated calls
 */
export async function getApiClientPublic(): Promise<AxiosInstance> {
  if (!apiClientPromise.public) {
    apiClientPromise.public = createApiClient(false)
  }
  return apiClientPromise.public
}
