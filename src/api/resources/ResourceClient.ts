import type { HydraResource, Resource, ResourceList } from '.'
import { getApiClient, removeEmpty } from '@/api'

export type CollectionParam = { page?: number; itemsPerPage?: number }

export type PartialWithoutId<T> = {
  [P in Exclude<keyof T, 'id'>]?: T[P]
}

export type ResourceClient<
  R extends Resource = Resource,
  CP extends CollectionParam = CollectionParam
> = {
  /**
   * Get one resource from its id
   */
  getItem?: (id: R['id']) => Promise<R>

  /**
   * Search for multiple resources
   */
  getCollection?: (params: CP) => Promise<ResourceList<R>>

  /**
   * Creates a new resource
   */
  create?: (resource: PartialWithoutId<R>) => Promise<R>

  /**
   * Partially updates current resource
   */
  patch?: (id: R['id'], model: PartialWithoutId<R>) => Promise<R>

  /**
   * Deletes the resource
   */
  delete?: (id: R['id']) => Promise<void>

  /**
   * Returns qualified ID if given is valid
   */
  getQualifiedId: (uuid: R['id']) => R['id']
}

export const REGEX_PARTIAL_UUID =
  /[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}/
export const REGEX_UUID = new RegExp(`^${REGEX_PARTIAL_UUID.source}$`)

class QualificationError extends Error {}

/**
 * Generic implementation of ResourceClient
 */
export abstract class AbstractResourceClient<
  R extends Resource,
  CP extends CollectionParam = CollectionParam
> implements ResourceClient<R, CP>
{
  /**
   * Resource endpoint
   */
  readonly endpoint: string

  /**
   * @inheritDoc
   */
  constructor(endpoint: string) {
    this.endpoint = endpoint
  }

  /**
   * @inheritDoc
   */
  async delete(uuid: R['id']): Promise<void> {
    const client = await getApiClient()
    await client.delete(this.getQualifiedId(uuid))
  }

  /**
   * @inheritDoc
   */
  async getItem(uuid: R['id']): Promise<R> {
    const client = await getApiClient()
    const { data } = await client.get(this.getQualifiedId(uuid))
    return this.cast(data)
  }

  /**
   * @inheritDoc
   */
  async getCollection(params: CP): Promise<ResourceList<R>> {
    const client = await getApiClient()

    if (!params.page) params.page = 1
    const { data } = await client.get(this.endpoint, {
      params: removeEmpty(params),
    })

    return {
      resources: data['hydra:member'].map(this.cast) ?? [],
      total: data['hydra:totalItems'] ?? 0,
      next: data?.['hydra:view']?.['hydra:next']
        ? async () =>
            await this.getCollection({
              ...params,
              page: (params.page || 1) + 1,
            })
        : undefined,
    }
  }

  /**
   * @inheritDoc
   */
  async patch(id: R['id'], model: PartialWithoutId<R>): Promise<R> {
    const client = await getApiClient()
    const { data } = await client.patch(this.getQualifiedId(id), model)
    return this.cast(data)
  }

  /**
   * @inheritDoc
   */
  async create(resource: PartialWithoutId<R>): Promise<R> {
    const client = await getApiClient()
    const { data } = await client.post(this.endpoint, resource)
    return this.cast(data)
  }

  /**
   * Casts API return object into APP object
   */
  protected abstract cast(obj: R & HydraResource): R

  /**
   * @inheritDoc
   */
  getQualifiedId(uuid: R['id']): R['id'] {
    if (
      new RegExp(`^${this.endpoint}/${REGEX_PARTIAL_UUID.source}$`).exec(uuid)
    )
      return uuid

    if (REGEX_UUID.exec(uuid)) return `${this.endpoint}/${uuid}`

    throw new QualificationError(
      `ID '${uuid}' should not be use with endpoint ${this.endpoint}`
    )
  }
}
