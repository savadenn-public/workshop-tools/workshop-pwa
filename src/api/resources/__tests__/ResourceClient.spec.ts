import { userClient } from '@/api'

describe('ResourceClient', () => {
  describe('ResourceClient.getQualifiedId', () => {
    test('pass with a qualified ID', () => {
      expect(
        userClient.getQualifiedId(
          '/auth/users/1ec7dd87-1775-6898-8f63-bbe43254a358'
        )
      ).toBe('/auth/users/1ec7dd87-1775-6898-8f63-bbe43254a358')
    })

    test('pass with an unqualified ID', () => {
      expect(
        userClient.getQualifiedId('1ec7dd87-1775-6898-8f63-bbe43254a358')
      ).toBe('/auth/users/1ec7dd87-1775-6898-8f63-bbe43254a358')
    })

    test('ko: invalid uuid', () => {
      expect(() => userClient.getQualifiedId('1ec7dd87-1775-6898')).toThrow(
        new Error(
          `ID '1ec7dd87-1775-6898' should not be use with endpoint /auth/users`
        )
      )

      expect(() =>
        userClient.getQualifiedId('/auth/users/1ec7dd87-1775-6898')
      ).toThrow(
        new Error(
          `ID '/auth/users/1ec7dd87-1775-6898' should not be use with endpoint /auth/users`
        )
      )
    })
  })
})
