import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import {
  BusinessCasesParams,
  businessCaseClient,
  BusinessCaseClient,
  BusinessCase,
} from '@/api/resources'

const mockAxios = new MockAdapter(axios, { onNoMatch: 'throwException' })
jest.mock('@/api/Client')

describe('BusinessCaseClient', () => {
  test('BusinessCaseClient.getCollection', async () => {
    mockAxios.onGet('/api/business_cases').reply(200, {
      'hydra:member': [{ '@id': 1 }, { '@id': 2 }, { '@id': 3 }],
      'hydra:totalItems': 3,
    })

    const actual = await businessCaseClient.getCollection({})
    const expected = {
      resources: [
        {
          id: 1,
          code: undefined,
          color: null,
          createdAt: undefined,
          description: undefined,
          dueDate: null,
          archiveDate: null,
        },
        {
          id: 2,
          code: undefined,
          color: null,
          createdAt: undefined,
          description: undefined,
          dueDate: null,
          archiveDate: null,
        },
        {
          id: 3,
          code: undefined,
          color: null,
          createdAt: undefined,
          description: undefined,
          dueDate: null,
          archiveDate: null,
        },
      ],
      total: 3,
    }

    expect(actual).toEqual(expected)
  })

  test('BusinessCaseClient.getItem', async () => {
    mockAxios
      .onGet('/api/business_cases/85a830e4-1185-451f-a125-1af93ecb2b32')
      .reply(200, {
        '@id': '/api/business_cases/85a830e4-1185-451f-a125-1af93ecb2b32',
        name: 'A business case',
        code: '666',
        description: 'Foo description',
      })

    const actual = await businessCaseClient.getItem(
      '/api/business_cases/85a830e4-1185-451f-a125-1af93ecb2b32'
    )

    const expected = {
      id: '/api/business_cases/85a830e4-1185-451f-a125-1af93ecb2b32',
      name: 'A business case',
      code: '666',
      description: 'Foo description',
      dueDate: null,
      color: null,
      archiveDate: null,
    }
    expect(actual).toEqual(expected)
  })

  test('BusinessCaseClient.create', async () => {
    const requestBody = {
      code: '666',
      name: 'Something',
      description: 'A description',
    }

    mockAxios.onPost('/api/business_cases', requestBody).reply(201, {
      '@id': 'new-uuid',
      code: '666',
      name: 'Something',
      description: 'A description',
    })

    const actual = await businessCaseClient.create(requestBody)

    const expected = {
      id: 'new-uuid',
      code: '666',
      color: null,
      name: 'Something',
      description: 'A description',
      dueDate: null,
      archiveDate: null,
    }
    expect(actual).toEqual(expected)
  })

  test('BusinessCaseClient.patch', async () => {
    const requestBody = {
      id: '/api/business_cases/85a830e4-1185-451f-a125-1af93ecb2b32',
      description: 'New description',
    }
    mockAxios
      .onPatch(
        '/api/business_cases/85a830e4-1185-451f-a125-1af93ecb2b32',
        requestBody
      )
      .reply(200, {
        '@id': '/api/business_cases/85a830e4-1185-451f-a125-1af93ecb2b32',
        name: 'Task',
        code: '666',
        description: 'New description',
      })

    const actual = await businessCaseClient.patch(
      '/api/business_cases/85a830e4-1185-451f-a125-1af93ecb2b32',
      requestBody
    )

    expect(actual).toEqual({
      code: '666',
      color: null,
      createdAt: undefined,
      description: 'New description',
      id: '/api/business_cases/85a830e4-1185-451f-a125-1af93ecb2b32',
      dueDate: null,
      name: 'Task',
      archiveDate: null,
    })
  })

  describe('BusinessClient.paramsToQueryParams', () => {
    test('test with dueDateBefore filter', () => {
      const now = new Date()
      const params: BusinessCasesParams = {
        page: 42,
        code: 'TX42',
        name: 'A name',
        dueDateBefore: now,
        'order[dueDate]': 'DESC',
      }
      const actual = BusinessCaseClient.paramsToQueryParams(params)

      const expected = {
        page: '42',
        code: 'TX42',
        name: 'A name',
        'dueDate[before]': now.toISOString(),
        'order[dueDate]': 'DESC',
      }

      expect(actual).toEqual(expected)
    })

    test('test with dueDateAfter filter', () => {
      const now = new Date()
      const params: BusinessCasesParams = {
        page: 12,
        code: 'CS26',
        name: 'Another',
        dueDateAfter: now,
        'order[dueDate]': 'ASC',
      }
      const actual = BusinessCaseClient.paramsToQueryParams(params)

      const expected = {
        page: '12',
        code: 'CS26',
        name: 'Another',
        'dueDate[after]': now.toISOString(),
        'order[dueDate]': 'ASC',
      }

      expect(actual).toEqual(expected)
    })
  })
})
