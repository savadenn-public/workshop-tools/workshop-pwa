import { getParentId, removeEmpty } from '@/api/resources'

describe('removeEmpty', () => {
  it('removes nullish and string empty attribute', () => {
    const objectToClear = {
      a: null,
      b: undefined,
      c: '',
      d: 0,
      e: false,
      f: 'something',
    }

    const actual = removeEmpty(objectToClear)
    const expected = {
      d: 0,
      e: false,
      f: 'something',
    }

    expect(actual).toEqual(expected)

    expect(removeEmpty(null)).toEqual({})
    expect(removeEmpty(undefined)).toEqual({})
  })
})
