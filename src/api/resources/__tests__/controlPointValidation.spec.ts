import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import { controlPointValidationClient } from '@/api/resources/controlPointValidation'

const mockAxios = new MockAdapter(axios, { onNoMatch: 'throwException' })
jest.mock('@/api/Client')

describe('ControlPointValidationClient', () => {
  test('ControlPointValidationClient.getCollection', async () => {
    mockAxios.onGet('/api/control_point_validations').reply(200, {
      'hydra:member': [
        {
          '@id': 1,
          createdAt: '2022-01-01 10:28:00',
          verifier: 'user-guid',
          controlPoint: 'cp-uuid',
        },
      ],
      'hydra:totalItems': 1,
    })

    const actual = await controlPointValidationClient.getCollection({})
    const expected = {
      resources: [
        {
          id: 1,
          createdAt: new Date('2022-01-01 10:28:00'),
          verifier: 'user-guid',
          controlPoint: 'cp-uuid',
        },
      ],
      total: 1,
    }

    expect(actual).toEqual(expected)
  })
})
