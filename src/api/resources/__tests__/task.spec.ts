import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import { taskClient, GetTasksParam, Task } from '@/api/resources'

const mockAxios = new MockAdapter(axios, { onNoMatch: 'throwException' })
jest.mock('@/api/Client')

describe('TaskClient', () => {
  test('TaskClient.getCollection', async () => {
    mockAxios.onGet('/api/tasks').reply(200, {
      'hydra:member': [
        { '@id': 1, createdAt: '2021-10-01T09:49:12+00:00' },
        { '@id': 2, createdAt: '2021-10-01T09:49:12+00:00' },
        { '@id': 3, createdAt: '2021-10-01T09:49:12+00:00' },
      ],
      'hydra:totalItems': 3,
    })

    const params: GetTasksParam = { page: 1, 'order[createdAt]': 'DESC' }
    const actual = await taskClient.getCollection(params)
    expect(actual).toEqual({
      resources: [
        {
          id: 1,
          assignedTo: null,
          businessCase: undefined,
          createdAt: new Date('2021-10-01T09:49:12Z'),
          description: undefined,
          durationMax: undefined,
          durationMin: undefined,
          startDate: undefined,
          title: undefined,
        },
        {
          id: 2,
          assignedTo: null,
          businessCase: undefined,
          createdAt: new Date('2021-10-01T09:49:12Z'),
          description: undefined,
          durationMax: undefined,
          durationMin: undefined,
          startDate: undefined,
          title: undefined,
        },
        {
          id: 3,
          assignedTo: null,
          businessCase: undefined,
          createdAt: new Date('2021-10-01T09:49:12Z'),
          description: undefined,
          durationMax: undefined,
          durationMin: undefined,
          startDate: undefined,
          title: undefined,
        },
      ],
      total: 3,
    })
  })

  test('TaskClient.getTask', async () => {
    mockAxios
      .onGet('/api/tasks/85a830e4-1185-451f-a125-1af93ecb2b32')
      .reply(200, {
        '@context': '/api/contexts/Task',
        '@id': '/api/tasks/85a830e4-1185-451f-a125-1af93ecb2b32',
        '@type': 'Task',
        id: 'foo-uuid',
        title: 'Foo title.',
        description: 'Foo description.',
        businessCase:
          '/api/business_cases/85a830e4-1185-451f-a326-1af93ecb2b14',
        createdAt: '2021-10-01T09:49:12+00:00',
      })

    const actual = await taskClient.getItem(
      '/api/tasks/85a830e4-1185-451f-a125-1af93ecb2b32'
    )

    expect(actual).toEqual<Task>({
      id: '/api/tasks/85a830e4-1185-451f-a125-1af93ecb2b32',
      title: 'Foo title.',
      description: 'Foo description.',
      businessCase: '/api/business_cases/85a830e4-1185-451f-a326-1af93ecb2b14',
      createdAt: new Date('2021-10-01T09:49:12Z'),
      assignedTo: null,
      durationMax: undefined,
      durationMin: undefined,
      startDate: undefined,
    })
  })

  test('TaskClient.patch', async () => {
    mockAxios
      .onPatch('/api/tasks/85a830e4-1185-451f-a125-1af93ecb2b32')
      .reply(200, {
        '@context': '/api/contexts/Task',
        '@id': '/api/tasks/85a830e4-1185-451f-a125-1af93ecb2b32',
        '@type': 'Task',
        id: 'foo-uuid',
        title: 'Updated title.',
        description: 'Foo description.',
        businessCase:
          '/api/business_cases/85a830e4-1185-451f-a326-1af93ecb2b14',
        createdAt: '2021-10-01T09:49:12+00:00',
      })

    const expected: Task = {
      id: '/api/tasks/85a830e4-1185-451f-a125-1af93ecb2b32',
      title: 'Updated title.',
      assignedTo: null,
      description: 'Foo description.',
      businessCase: '/api/business_cases/85a830e4-1185-451f-a326-1af93ecb2b14',
      createdAt: new Date('2021-10-01T09:49:12+00:00'),
      startDate: undefined,
      durationMin: undefined,
      durationMax: undefined,
    }

    const actual = await taskClient.patch(
      '/api/tasks/85a830e4-1185-451f-a125-1af93ecb2b32',
      {
        title: 'Updated title.',
      }
    )

    expect(actual).toEqual(expected)
  })

  test('TaskClient.create', async () => {
    mockAxios.onPost('/api/tasks').reply(201, {
      '@context': '/api/contexts/Task',
      '@id': '/api/tasks/t-uuid',
      '@type': 'Task',
      id: 't-uuid',
      title: 'New task',
      businessCase: '/api/business_cases/bc-uuid',
      createdAt: '2021-10-15T12:43:34+00:00',
    })

    const expected: Task = {
      id: '/api/tasks/t-uuid',
      title: 'New task',
      assignedTo: null,
      description: undefined,
      businessCase: '/api/business_cases/bc-uuid',
      createdAt: new Date('2021-10-15T12:43:34+00:00'),
      startDate: undefined,
      durationMin: undefined,
      durationMax: undefined,
    }

    const actual = await taskClient.create({
      title: 'Updated title.',
      businessCase: '/api/business_cases/bc-uuid',
    })

    expect(actual).toEqual(expected)
  })

  test('TaskClient.delete', async () => {
    mockAxios
      .onDelete('/api/tasks/85a830e4-1185-451f-a125-1af93ecb2b32')
      .reply(204)

    await expect(
      taskClient.delete('/api/tasks/85a830e4-1185-451f-a125-1af93ecb2b32')
    ).resolves.toBe(undefined)
  })
})
