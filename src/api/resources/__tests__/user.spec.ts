import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import { userClient } from '@/api/resources'

const mockAxios = new MockAdapter(axios, { onNoMatch: 'throwException' })
jest.mock('@/api/Client')

describe('UserClient', () => {
  it('UserClient.getItem', async () => {
    mockAxios
      .onGet('/auth/users/1ec7dd87-1775-6898-8f63-bbe43254a358')
      .reply(200, {
        '@id': '/auth/users/1ec7dd87-1775-6898-8f63-bbe43254a358',
        email: 'my@mail.example',
      })

    const actual = await userClient.getItem(
      '/auth/users/1ec7dd87-1775-6898-8f63-bbe43254a358'
    )

    expect(actual).toEqual({
      id: '/auth/users/1ec7dd87-1775-6898-8f63-bbe43254a358',
      name: '',
      email: 'my@mail.example',
      roles: [],
      avatar: null,
      lockDate: null,
    })
  })

  it('UserClient.patch', async () => {
    const requestBody = {
      id: '/auth/users/1ec7dd87-1775-6898-8f63-bbe43254a358',
      name: 'My New Name',
    }

    mockAxios
      .onPatch('/auth/users/1ec7dd87-1775-6898-8f63-bbe43254a358', requestBody)
      .reply(200, {
        '@id': '/auth/users/1ec7dd87-1775-6898-8f63-bbe43254a358',
        name: 'My New Name',
        email: 'my@mail.example',
        roles: ['MY_ROLE'],
      })

    const actual = await userClient.patch(
      '/auth/users/1ec7dd87-1775-6898-8f63-bbe43254a358',
      requestBody
    )

    expect(actual).toEqual({
      id: '/auth/users/1ec7dd87-1775-6898-8f63-bbe43254a358',
      name: 'My New Name',
      email: 'my@mail.example',
      roles: ['MY_ROLE'],
      avatar: null,
      lockDate: null,
    })
  })

  it('UserClient.consumeConfirmEmailToken', async () => {
    mockAxios
      .onPost('/auth/confirm_email', {
        token: 'foo-token',
      })
      .reply(200)

    await expect(
      userClient.consumeConfirmEmailToken('foo-token')
    ).resolves.toBe(undefined)
  })
})
