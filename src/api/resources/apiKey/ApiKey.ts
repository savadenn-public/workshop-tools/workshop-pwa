import { Resource } from '@/api'

export type ApiKey = Resource & {
  usage: string
}

export type ApiKeyPlainToken = ApiKey & {
  plainToken: string
}
