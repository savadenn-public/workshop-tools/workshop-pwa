import { AbstractResourceClient, HydraResource, PartialWithoutId } from '..'
import type { ApiKey, ApiKeyPlainToken } from '.'
import { getApiClient } from '@/api'

class ApiKeyClient extends AbstractResourceClient<ApiKey> {
  /**
   * @inheritDoc
   */
  protected cast(obj: ApiKey & HydraResource): ApiKey {
    return {
      id: obj['@id'],
      usage: obj.usage,
    }
  }

  /**
   * @inheritDoc
   */
  async create(resource: PartialWithoutId<ApiKey>): Promise<ApiKeyPlainToken> {
    const client = await getApiClient()
    const { data } = await client.post(this.endpoint, resource)

    return { ...this.cast(data), plainToken: data.plainToken }
  }
}

export const apiKeyClient = new ApiKeyClient('/auth/api_keys')
