import { Resource } from '@/api'

export type EarSize = 'small' | 'big'
export type EarringType = 'none' | 'small' | 'big'
export type EyebrowType = 'up' | 'down' | 'eyelashesUp' | 'eyelashesDown'
export type EyesType = 'circle' | 'oval' | 'smile' | 'shadow'
export type GlassesType = 'none' | 'round' | 'square' | 'pilote'
export type HatType = 'none' | 'beanie' | 'turban' | 'top'
export type HairType =
  | 'crew'
  | 'thick'
  | 'mohawk'
  | 'feathered'
  | 'bob'
  | 'pixie'
  | 'none'
export type MouthType =
  | 'surprised'
  | 'laugh'
  | 'smile'
  | 'smirk'
  | 'pucker'
  | 'nervous'
export type NoseType = 'curved' | 'round' | 'pointed'
export type ShirtType = 'polo' | 'crew' | 'tee'

export type AvatarConfig = {
  hatColor: string
  faceColor: string
  hairColor: string
  shirtColor: string
  earSize: EarSize
  earring: EarringType
  hatType: HatType
  eyeType: EyesType
  eyebrowType: EyebrowType
  hairType: HairType
  noseType: NoseType
  mouthType: MouthType
  shirtType: ShirtType
  glassesType: GlassesType
}

export type Avatar = Resource & AvatarConfig
