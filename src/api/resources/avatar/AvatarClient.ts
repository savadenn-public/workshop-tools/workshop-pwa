import type { Avatar } from '.'
import type { HydraResource } from '..'
import { AbstractResourceClient } from '..'

class AvatarClient extends AbstractResourceClient<Avatar> {
  /**
   * Returns qualified Workforce
   */
  protected cast(obj: Avatar & HydraResource): Avatar {
    return {
      earSize: obj.earSize,
      eyeType: obj.eyeType,
      eyebrowType: obj.eyebrowType,
      faceColor: obj.faceColor,
      glassesType: obj.glassesType,
      hairColor: obj.hairColor,
      hairType: obj.hairType,
      hatColor: obj.hatColor,
      hatType: obj.hatType,
      mouthType: obj.mouthType,
      noseType: obj.noseType,
      shirtColor: obj.shirtColor,
      shirtType: obj.shirtType,
      id: obj['@id'],
      earring: obj.earring,
    }
  }
}

export const avatarClient = new AvatarClient('/auth/avatars')
