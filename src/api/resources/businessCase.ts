import type { CollectionParam, HydraResource, Task } from '.'
import { AbstractWatchableResourceClient } from '.'
import { getApiClient } from '@/api'

export type BusinessCase = {
  id: string
  code?: string
  name?: string
  color: string | null
  description?: string
  createdAt: Date
  archiveDate: Date | null
  dueDate: Date | null
  tasks: Array<Task['id']>
}

/**
 * Params used when fetching a collection of business-case resources
 */
export type BusinessCasesParams = CollectionParam &
  Partial<{
    code: string
    name: string
    dueDateBefore: Date
    dueDateAfter: Date
    'order[dueDate]': 'ASC' | 'DESC'
    status: 'open' | 'archived'
  }>

export class BusinessCaseClient extends AbstractWatchableResourceClient<
  BusinessCase,
  BusinessCasesParams
> {
  /**
   * Make a copy of a business case
   *
   * @param uuid Resource ID to copy
   * @param name Name of the new business case
   * @param code Code of the new business case
   */
  async clone(
    uuid: BusinessCase['id'],
    name: string,
    code: string
  ): Promise<BusinessCase> {
    const api = await getApiClient()
    const { data } = await api.post(uuid + '/clone', {
      name: name,
      code: code,
    })

    return this.cast(data)
  }

  /**
   * Returns qualified BusinessCase
   */
  cast(obj: BusinessCase & HydraResource): BusinessCase {
    return {
      id: obj['@id'],
      code: obj.code,
      name: obj.name,
      color: obj.color || null,
      description: obj.description,
      createdAt: obj.createdAt,
      dueDate: obj.dueDate ? new Date(obj.dueDate) : null,
      archiveDate: obj.archiveDate ? new Date(obj.archiveDate) : null,
      tasks: obj.tasks,
    }
  }

  /**
   * Convert given params into API query params
   */
  static paramsToQueryParams(
    params: BusinessCasesParams
  ): Record<string, string> {
    const queryParams: Record<string, string> = {}

    if (params.page) queryParams.page = params.page.toString()
    if (params.code) queryParams.code = params.code
    if (params.name) queryParams.name = params.name
    if (params['order[dueDate]']) {
      queryParams['order[dueDate]'] = params['order[dueDate]']
    }
    if (params.dueDateBefore) {
      queryParams['dueDate[before]'] = params.dueDateBefore.toISOString()
    }
    if (params.dueDateAfter) {
      queryParams['dueDate[after]'] = params.dueDateAfter.toISOString()
    }

    return queryParams
  }
}

export const businessCaseClient = new BusinessCaseClient('/api/business_cases')
