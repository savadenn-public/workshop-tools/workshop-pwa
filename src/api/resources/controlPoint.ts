import {
  AbstractResourceClient,
  CollectionParam,
  HydraResource,
  Task,
} from '@/api'
import { ControlPointValidation } from '@/api/resources/controlPointValidation'

export type ControlPoint = {
  id: string
  description: string
  createdAt: Date
  task: Task['id']
  controlPointValidations: Array<ControlPointValidation>
}

export type ControlPointParams = CollectionParam & {
  task?: Task['id']
}

export class ControlPointClient extends AbstractResourceClient<
  ControlPoint,
  ControlPointParams
> {
  protected cast(obj: ControlPoint & HydraResource): ControlPoint {
    return {
      id: obj['@id'],
      description: obj.description,
      createdAt: new Date(obj.createdAt),
      task: obj.task,
      controlPointValidations: obj.controlPointValidations,
    }
  }
}
export const controlPointClient = new ControlPointClient('/api/control_points')
