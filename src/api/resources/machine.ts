import {
  HydraResource,
  CollectionParam,
  ResourceList,
  AbstractResourceClient,
} from '.'

export type Machine = {
  id: string
  name: string
}

class MachineClient extends AbstractResourceClient<Machine> {
  /**
   * Returns qualified Workforce
   */
  protected cast(obj: Machine & HydraResource): Machine {
    return {
      id: obj['@id'],
      name: obj.name,
    }
  }
}

export const machineClient = new MachineClient('/api/machines')
