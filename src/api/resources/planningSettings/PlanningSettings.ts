import type { Resource } from '..'

export const BY_BUSINESS_CASE = 'by_business_case'
export const BY_WORK_FORCE = 'by_workforce'
export type PlanningSettingsModes =
  | typeof BY_BUSINESS_CASE
  | typeof BY_WORK_FORCE

export type PlanningSettings = {
  id: Resource['id']
  name: string
  owner: Resource['id']
  blockWidth: number
  mode: PlanningSettingsModes
  daysNumber: number
  granularity: number
  selectedWorkforces: Array<Resource['id']>
}
