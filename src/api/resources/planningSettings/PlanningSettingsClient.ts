import type { HydraResource } from '..'
import { AbstractResourceClient } from '..'

import { PlanningSettings } from '@/api/resources/planningSettings/PlanningSettings'

class PlanningSettingsClient extends AbstractResourceClient<PlanningSettings> {
  /**
   * @override
   */
  protected cast(obj: PlanningSettings & HydraResource): PlanningSettings {
    return {
      id: obj['@id'],
      name: obj.name,
      blockWidth: obj.blockWidth,
      mode: obj.mode,
      granularity: obj.granularity,
      daysNumber: obj.daysNumber,
      owner: obj.owner,
      selectedWorkforces: obj.selectedWorkforces || [],
    }
  }
}

export const planningSettingsClient = new PlanningSettingsClient(
  '/api/planning_settings'
)
