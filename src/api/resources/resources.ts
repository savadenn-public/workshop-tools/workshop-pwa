import {
  apiKeyClient,
  businessCaseClient,
  machineClient,
  Resource,
  taskClient,
  teamClient,
  userClient,
  workforceClient,
} from '.'

/**
 * Regex to get resource name
 */
const RESOURCES_MATCHING = /^(?<type>\/[^/]+\/[^/]+)\/[^/]+$/

export enum ResourceType {
  User,
  Task,
  BusinessCase,
  team,
  machine,
  workforce,
  apiKey,
}

/**
 * Key to resource type
 */
const URI_TO_RESOURCE: Record<string, ResourceType> = {
  [taskClient.endpoint]: ResourceType.Task,
  [userClient.endpoint]: ResourceType.User,
  [businessCaseClient.endpoint]: ResourceType.BusinessCase,
  [teamClient.endpoint]: ResourceType.team,
  [machineClient.endpoint]: ResourceType.machine,
  [workforceClient.endpoint]: ResourceType.workforce,
  [apiKeyClient.endpoint]: ResourceType.apiKey,
}

/**
 * Get the type of resource
 */
export function getType<T extends Resource>(
  resource: T
): ResourceType | undefined {
  const type = resource.id.match(RESOURCES_MATCHING)?.groups?.type
  if (!type) return
  return URI_TO_RESOURCE[type]
}

/**
 * ParentId property key
 */
const PARENT_KEY: Record<ResourceType, string | undefined> = {
  [ResourceType.Task]: 'businessCase',
  [ResourceType.User]: undefined,
  [ResourceType.BusinessCase]: undefined,
  [ResourceType.team]: undefined,
  [ResourceType.machine]: undefined,
  [ResourceType.workforce]: undefined,
  [ResourceType.apiKey]: undefined,
}

/**
 * Retrieves parentId if exist and relevant
 */
export function getParentId<T extends Resource>(
  resource: T
): Resource['id'] | undefined {
  const type = getType(resource)
  if (!type) return
  const attribute = PARENT_KEY[type]
  if (!attribute) return
  return Object.getOwnPropertyDescriptor(resource, attribute)?.value as string
}
