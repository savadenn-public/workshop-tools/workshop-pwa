import type {
  Resource,
  HydraResource,
  CollectionParam,
  BusinessCase,
  Workforce,
} from '.'
import { AbstractWatchableResourceClient } from '.'

/**
 * Default duration of a task in minutes
 */
export const DEFAULT_DURATION = 24 * 60

export type Task = {
  id: string
  title: string
  description?: string | null
  createdAt: Date

  /**
   * Parent business case of this task
   */
  businessCase: Resource['id']

  /**
   * OrganisationResource assigned to this task
   */
  assignedTo: Resource['id'] | null

  /**
   * Start date of the task
   */
  startDate?: Date | null

  /**
   * End Date of the task
   */
  endDate?: Date | null

  /**
   * Minimum estimated duration in minutes
   */
  durationMin?: number

  /**
   * Maximum estimated duration in minutes
   */
  durationMax?: number

  /**
   * Estimated end date for minimal duration
   */
  estimatedEndMin?: Date

  /**
   * Estimated end date for maximal duration
   */
  estimatedEndMax?: Date
}

export type GetTasksParam = CollectionParam &
  Partial<{
    'order[createdAt]': 'ASC' | 'DESC'
    'startDate[before]': string
    'startDate[strictly_before]': string
    'startDate[after]': string
    'startDate[strictly_after]': string
    'period[from]': string
    'period[to]': string
    'exists[startDate]': boolean
    title: string
    businessCase: BusinessCase['id']
    assignedTo: Array<Workforce['id']>
    archived?: boolean
  }>

class TaskClient extends AbstractWatchableResourceClient<Task, GetTasksParam> {
  /**
   * Returns qualified Workforce
   */
  protected cast(obj: Task & HydraResource): Task {
    return {
      id: obj['@id'],
      title: obj.title,
      description: obj.description,
      createdAt: new Date(obj.createdAt),
      businessCase: obj.businessCase,
      assignedTo: obj.assignedTo ?? null,
      startDate: obj.startDate ? new Date(obj.startDate) : undefined,
      endDate: obj.endDate ? new Date(obj.endDate) : undefined,
      durationMin: obj.durationMin,
      durationMax: obj.durationMax,
      estimatedEndMin: obj.estimatedEndMin
        ? new Date(obj.estimatedEndMin)
        : undefined,
      estimatedEndMax: obj.estimatedEndMax
        ? new Date(obj.estimatedEndMax)
        : undefined,
    }
  }
}

export const taskClient = new TaskClient('/api/tasks')
