import { Resource, HydraResource, AbstractResourceClient } from '.'

export type Team = {
  id: string
  name: string
  users: Array<Resource['id']>
}

class TeamClient extends AbstractResourceClient<Team> {
  /**
   * Returns qualified Workforce
   */
  protected cast(obj: Team & HydraResource): Team {
    return {
      id: obj['@id'],
      name: obj.name,
      users: obj.users ?? [],
    }
  }
}

export const teamClient = new TeamClient('/api/teams')
