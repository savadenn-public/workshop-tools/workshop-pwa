import type { HydraResource, Resource, Role } from '.'
import { AbstractResourceClient } from '.'
import { getApiClientPublic } from '@/api'

export type User = {
  id: string
  name: string
  email: string
  roles: Role[]
  lockDate: Date | null
  avatar: Resource['id'] | null
}

class UserClient extends AbstractResourceClient<User> {
  /**
   * Returns qualified User
   */
  protected cast(obj: User & HydraResource): User {
    return {
      id: obj['@id'],
      name: obj.name ?? '',
      email: obj.email,
      roles: obj.roles ?? [],
      avatar: obj.avatar ?? null,
      lockDate: obj.lockDate ? new Date(obj.lockDate) : null,
    }
  }

  /**
   * Consume a change-email token in order to update user email
   */
  async consumeConfirmEmailToken(token: string): Promise<void> {
    const client = await getApiClientPublic()
    await client.post('/auth/confirm_email', { token: token })
  }
}

export const userClient = new UserClient('/auth/users')
