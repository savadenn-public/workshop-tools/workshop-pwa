import type { ChangeHandler, StopHandler, WatchableResourceClient } from '.'
import type { CollectionParam, Resource } from '..'
import { AbstractResourceClient } from '..'
import { getApiClient } from '@/api/Client'

const MERCURE_URL = window.location.origin + '/.well-known/mercure'

export abstract class AbstractWatchableResourceClient<
    R extends Resource,
    CP extends CollectionParam = CollectionParam
  >
  extends AbstractResourceClient<R, CP>
  implements WatchableResourceClient<R, CP>
{
  /**
   * Creates a watcher for given topic
   */
  protected async createWatcher(
    topic: string,
    changeHandler: ChangeHandler<R>
  ): Promise<StopHandler> {
    const client = await getApiClient()
    await client.get('/auth/mercure/discovery')

    const url = new URL(MERCURE_URL)
    url.searchParams.append('topic', topic)
    const watcher = new EventSource(url)
    watcher.onmessage = async (e: { data: string }) => {
      const value = JSON.parse(e.data)
      if (value['@id']) changeHandler(value['@id'])
    }

    return () => watcher.close()
  }

  /**
   * @inheritDoc
   */
  async watchAll(changeHandler: ChangeHandler<R>): Promise<StopHandler> {
    return this.createWatcher(
      `${window.location.origin}${this.endpoint}/{id}`,
      changeHandler
    )
  }

  /**
   * @inheritDoc
   */
  async watchItem(
    id: R['id'],
    changeHandler: ChangeHandler<R>
  ): Promise<StopHandler> {
    return this.createWatcher(`${window.location.origin}${id}`, changeHandler)
  }
}
