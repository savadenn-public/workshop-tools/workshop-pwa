import type { ResourceClient, CollectionParam, Resource } from '..'

/**
 * Method to stop a watcher
 */
export type StopHandler = () => void

/**
 * Handle a change on a resource
 */
export type ChangeHandler<R extends Resource> = (
  id: R['id']
) => void | Promise<void>

export type WatchableResourceClient<
  R extends Resource = Resource,
  CP extends CollectionParam = CollectionParam
> = ResourceClient<R, CP> & {
  /**
   * Watch a specific resource for change
   *
   * @return a function to stop watching
   */
  watchItem: (
    id: R['id'],
    changeHandler: ChangeHandler<R>
  ) => Promise<StopHandler>

  /**
   * Watch changes on all resources
   *
   * @return a function to stop watching
   */
  watchAll: (changeHandler: ChangeHandler<R>) => Promise<StopHandler>
}
