import {
  HydraResource,
  AbstractResourceClient,
  PartialWithoutId,
  REGEX_PARTIAL_UUID,
} from '.'

export type Workforce = {
  id: string
  name: string
}

class WorkforceClient extends AbstractResourceClient<Workforce> {
  async create(resource: PartialWithoutId<Workforce>): Promise<Workforce> {
    throw new Error('Cannot create workforce, either create Teams or Machines')
  }

  /**
   * Returns qualified Workforce
   */
  cast(obj: Workforce & HydraResource): Workforce {
    return {
      id: obj['@id'],
      name: obj.name,
    }
  }

  /**
   * Qualifies given ID for current resource if not
   * @param uuid
   */
  getQualifiedId(uuid: Workforce['id']): Workforce['id'] {
    if (
      new RegExp(`^/api/(teams|machines)/${REGEX_PARTIAL_UUID.source}$`).exec(
        uuid
      )
    )
      return uuid

    return super.getQualifiedId(uuid)
  }
}

export const workforceClient = new WorkforceClient('/api/workforces')
