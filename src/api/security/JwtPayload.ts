import { JwtPayload as Base } from 'jwt-decode'
import type { Role } from '@/api'

export type JwtPayload = Base & {
  guid: string
  roles: Role[]
}
