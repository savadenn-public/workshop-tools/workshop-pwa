import type { JwtPayload } from './JwtPayload'
import { settings, roleManager } from '@/states'
import jwtDecode from 'jwt-decode'
import { refreshTokens } from '@/api/security'
import { userClient } from '@/api'

const KEY = 'token'
const MINIMUM_VALIDITY_REMAINING = 60

/**
 * Get token for API call
 */
export async function getToken(): Promise<string | null> {
  let token = sessionStorage.getItem(KEY)

  const renew = async () => {
    await refreshTokens()
    token = sessionStorage.getItem(KEY)
  }

  // Verify token exists
  if (!token) await renew()
  if (!token) return null

  // Verify token is valid
  let payload: JwtPayload | null
  try {
    payload = jwtDecode<JwtPayload>(token)
  } catch (e) {
    await renew()
    payload = jwtDecode<JwtPayload>(token)
  }

  // Verify token is not close to expiration
  if (
    payload.exp &&
    payload.exp - Date.now() / 1000 < MINIMUM_VALIDITY_REMAINING
  )
    await renew()

  return token
}

/**
 * Save token for API call
 * @param jwt
 */
export function setToken(jwt: string | null): void {
  if (jwt) {
    sessionStorage.setItem(KEY, jwt)
    try {
      const payload = jwtDecode<JwtPayload>(jwt)
      settings.user.id = userClient.getQualifiedId(payload.guid)
      return
    } catch (e) {
      console.error('Invalid JWT')
    }
  }
  sessionStorage.removeItem(KEY)
}

/**
 * Load roles from JWT
 */
export function loadRolesFromToken(): void {
  try {
    const jwt = sessionStorage.getItem(KEY)
    if (jwt) {
      const payload = jwtDecode<JwtPayload>(jwt)
      roleManager.roles.value = payload.roles
    }
  } catch (e) {
    console.error('Invalid JWT')
  }
}
