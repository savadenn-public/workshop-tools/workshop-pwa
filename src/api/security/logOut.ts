import { resetSettings, settings } from '@/states'
import { setToken } from './Token'
import { getApiClient } from '@/api'
import { unref } from 'vue'

export const REVOKE_ENDPOINT = '/auth/logout'

/**
 * Clear credentials
 */
export async function logOut(wipeSettings = true): Promise<void> {
  // Only revoke in case of wipe out
  if (wipeSettings) {
    const refreshToken = unref(settings.refreshToken)
    resetSettings()

    try {
      const client = await getApiClient()
      await client.post(REVOKE_ENDPOINT, {
        refresh_token: refreshToken,
      })
    } catch (exception) {
      console.error(exception)
    }
  } else {
    settings.refreshToken = null
  }
  setToken(null)
}
