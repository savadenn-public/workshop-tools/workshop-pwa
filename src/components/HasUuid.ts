import { computed, ComputedRef, getCurrentInstance } from 'vue'
import { v4 as uuid4 } from 'uuid'

/**
 * Returns object to spread in setup of a component that as an internal id
 */
export function hasUuid(): { uuid: ComputedRef<string> } {
  return {
    uuid: computed(() => {
      const cid = getCurrentInstance()
      /* istanbul ignore next */
      return cid ? cid.uid.toString() : uuid4()
    }),
  }
}
