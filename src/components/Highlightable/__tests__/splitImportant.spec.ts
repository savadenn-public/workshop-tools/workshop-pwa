import { splitImportant } from '@/components/Highlightable/splitImportant'

describe('splitImportant', () => {
  test('empty params', () => {
    expect(splitImportant('foo', null)).toEqual([
      { text: 'foo', important: false },
    ])
    expect(splitImportant('foo', null)).toEqual([
      { text: 'foo', important: false },
    ])

    expect(splitImportant('foo', '')).toEqual([
      { text: 'foo', important: false },
    ])
  })

  test('one occurrence', () => {
    expect(splitImportant('foobar', 'ob')).toEqual([
      { text: 'fo', important: false },
      { text: 'ob', important: true },
      { text: 'ar', important: false },
    ])
    expect(splitImportant('foobar', 'fo')).toEqual([
      { text: 'fo', important: true },
      { text: 'obar', important: false },
    ])

    expect(splitImportant('foobar', 'ar')).toEqual([
      { text: 'foob', important: false },
      { text: 'ar', important: true },
    ])
  })

  test('case insensitive', () => {
    expect(splitImportant('FoObar', 'ob')).toEqual([
      { text: 'Fo', important: false },
      { text: 'Ob', important: true },
      { text: 'ar', important: false },
    ])

    expect(splitImportant('FoObar', 'ob', true)).toEqual([
      { text: 'FoObar', important: false },
    ])

    expect(splitImportant('FoObar', 'Ob', true)).toEqual([
      { text: 'Fo', important: false },
      { text: 'Ob', important: true },
      { text: 'ar', important: false },
    ])
  })
})
