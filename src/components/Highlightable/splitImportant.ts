type Part = { text: string; important: boolean }

export function splitImportant(
  text: string,
  importantPattern: string | null | undefined,
  caseSensitive = false
): Part[] {
  if (!importantPattern) return [{ text, important: false }]

  const compareText = caseSensitive ? text : text.toLocaleLowerCase()
  const comparePattern = caseSensitive
    ? importantPattern
    : importantPattern.toLocaleLowerCase()

  const cuttingPattern = compareText.split(comparePattern)

  const parts: Part[] = []

  for (const part of cuttingPattern) {
    // if empty string means text start with pattern
    if (part !== '') {
      parts.push({
        text: text.substring(0, part.length),
        important: false,
      })
      text = text.substring(part.length)
    }

    if (text.length) {
      parts.push({
        text: text.substring(0, importantPattern.length),
        important: true,
      })
      text = text.substring(importantPattern.length)
    }
  }

  return parts
}
