import {
  EarringType,
  EarSize,
  EyebrowType,
  EyesType,
  GlassesType,
  HairType,
  HatType,
  MouthType,
  NoseType,
  ShirtType,
} from '@/api'

export type Option<T> = {
  value: T
  label: string
}

/**
 * Generates avatar part options from a list of values
 * @param part
 * @param values
 */
function generateOptions<T>(part: string, values: T[]): Option<T>[] {
  return values.map((value) => ({
    label: `component.avatar.${part}.${value}`,
    value,
  }))
}

export const AVATAR_OPTIONS = {
  earSizes: generateOptions<EarSize>('ear', ['small', 'big']),
  earringTypes: generateOptions<EarringType>('earring', [
    'none',
    'small',
    'big',
  ]),
  eyebrowTypes: generateOptions<EyebrowType>('eyebrow', [
    'up',
    'down',
    'eyelashesUp',
    'eyelashesDown',
  ]),
  eyeTypes: generateOptions<EyesType>('eye', [
    'circle',
    'oval',
    'smile',
    'shadow',
  ]),
  glassesTypes: generateOptions<GlassesType>('glasses', [
    'none',
    'round',
    'square',
    'pilote',
  ]),
  hairTypes: generateOptions<HairType>('hair', [
    'thick',
    'mohawk',
    'feathered',
    'bob',
    'pixie',
    'crew',
    'none',
  ]),
  hatTypes: generateOptions<HatType>('hat', [
    'none',
    'beanie',
    'turban',
    'top',
  ]),
  mouthTypes: generateOptions<MouthType>('mouth', [
    'surprised',
    'laugh',
    'smile',
    'smirk',
    'pucker',
    'nervous',
  ]),
  noseTypes: generateOptions<NoseType>('nose', ['curved', 'round', 'pointed']),
  shirtTypes: generateOptions<ShirtType>('shirt', ['crew', 'tee', 'polo']),
  faceColors: [
    '#fdd835',
    '#fddcac',
    '#f0c27e',
    '#e1ac6a',
    '#c68641',
    '#8d5624',
    '#6f3b11',
    '#401b01',
  ],
  shirtColors: [
    '#5c110a',
    '#00355c',
    '#8f2d7f',
    '#5a6e80',
    '#21791b',
    '#864e00',
    '#bea90a',
    '#8c2424',
  ],
  hatColors: ['#fefefe', '#737373', '#3556de'],
  hairColors: [
    '#a78055',
    '#c4a56c',
    '#da9e62',
    '#86220a',
    '#4f2a10',
    '#372211',
    '#4f2624',
    '#342625',
    '#1e1e1c',
  ],
}
