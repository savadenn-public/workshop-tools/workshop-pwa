import type { AvatarConfig } from '@/api'
import { AVATAR_OPTIONS } from '@/components/avatar/AvatarOptions'

export function getRandomAvatar(): AvatarConfig {
  return {
    eyebrowType: getRandomFromList(AVATAR_OPTIONS.eyebrowTypes).value,
    earSize: getRandomFromList(AVATAR_OPTIONS.earSizes).value,
    earring:
      getNoneHlfTheTime() ??
      getRandomFromList(AVATAR_OPTIONS.earringTypes).value,
    eyeType: getRandomFromList(AVATAR_OPTIONS.eyeTypes).value,
    faceColor: getRandomFromList(AVATAR_OPTIONS.faceColors),
    glassesType:
      getNoneHlfTheTime() ??
      getRandomFromList(AVATAR_OPTIONS.glassesTypes).value,
    hairColor: getRandomFromList(AVATAR_OPTIONS.hairColors),
    hairType: getRandomFromList(AVATAR_OPTIONS.hairTypes).value,
    hatColor: getRandomFromList(AVATAR_OPTIONS.hatColors),
    hatType:
      getNoneHlfTheTime() ?? getRandomFromList(AVATAR_OPTIONS.hatTypes).value,
    mouthType: getRandomFromList(AVATAR_OPTIONS.mouthTypes).value,
    noseType: getRandomFromList(AVATAR_OPTIONS.noseTypes).value,
    shirtColor: getRandomFromList(AVATAR_OPTIONS.shirtColors),
    shirtType: getRandomFromList(AVATAR_OPTIONS.shirtTypes).value,
  }
}

function getRandomFromList<T>(values: T[]): T {
  return values[Math.floor(Math.random() * values.length)]
}

function getNoneHlfTheTime() {
  return Math.random() > 0.5 ? 'none' : null
}
