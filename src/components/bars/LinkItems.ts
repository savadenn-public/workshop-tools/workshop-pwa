import type { LinkItem } from '@/components/bars/MenuItem'
import { computed } from 'vue'
import { ROUTES } from '@/plugins'
import { injectStrict } from '@/injectStrict'
import { RoleManagerKey } from '@/provided'

export const Home: LinkItem = {
  class: 'Menu__Home',
  label: 'page.home.title',
  icon: 'mdi mdi-home',
  to: ROUTES.HOME,
}

export const Settings: LinkItem = {
  class: 'Menu__Settings',
  label: 'page.settings.title',
  icon: 'mdi mdi-cog-outline',
  to: ROUTES.SETTINGS,
}

export const BusinessCases: LinkItem = {
  class: 'Menu__BusinessCase',
  label: 'page.business_cases.title',
  icon: 'mdi mdi-handshake-outline',
  to: ROUTES.BUSINESS_CASES,
}

export const WeekPlanning: LinkItem = {
  class: 'Menu__WeekPlanning',
  label: 'page.weekPlanning.title',
  icon: 'mdi mdi-calendar-week',
  to: ROUTES.WEEK_PLANNING,
}

export const Teams: LinkItem = {
  class: 'Menu__Teams',
  label: 'page.teams.title',
  icon: 'mdi mdi-account-multiple-outline',
  to: ROUTES.TEAMS,
}

export const Machines: LinkItem = {
  class: 'Menu__Machines',
  label: 'page.machines.title',
  icon: 'mdi mdi-robot-industrial',
  to: ROUTES.MACHINES,
}

export const Users: LinkItem = {
  class: 'Menu__Users',
  label: ROUTES.USERS.name,
  icon: 'mdi mdi-account-box-multiple-outline',
  to: ROUTES.USERS,
}

export const ApiKeys: LinkItem = {
  class: 'Menu__ApiKeys',
  label: ROUTES.APIKEYS.name,
  icon: 'mdi mdi-shield-key-outline',
  to: ROUTES.APIKEYS,
}

export const Administration: LinkItem = {
  class: 'Menu__Administration',
  label: 'page.administration.title',
  icon: 'mdi mdi-shield-crown-outline',
  to: ROUTES.ADMINISTRATION,
  items: [Users, Teams, Machines],
  key: 'admin',
}

// Common items of both sidebars (mobile & desktop)
export const mainMenuItems = computed(() => {
  const items = [WeekPlanning, BusinessCases]
  const roleManager = injectStrict(RoleManagerKey)
  if (roleManager.isAdmin.value) items.push(Administration)
  return items
})
