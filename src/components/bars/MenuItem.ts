import { i18n } from '@/types'

/**
 * @see https://primefaces.org/primevue/showcase/#/menumodel
 */
export interface MenuItem {
  class: string
  label: string
  icon: string
  key?: string
  items?: MenuItem[]
}

export type LinkItem = MenuItem & {
  to: { name: string; params?: { uuid: string } }
}

export type ActionItem = MenuItem & {
  command?: (event: Event) => void
}

/**
 * Translates label of given menu
 */
export function translate(menuItem: MenuItem, t: i18n.t): MenuItem {
  const result = {
    ...menuItem,
  }
  if (menuItem.label) {
    result.label = t(menuItem.label)
  }
  return result
}
