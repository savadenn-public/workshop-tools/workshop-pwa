import { cloneDeep, isEqual } from 'lodash'

export interface Recorder<T> {
  /**
   * Snapshot of target as reference
   */
  reference: T

  /**
   * Delta object between reference and target
   */
  changes: Partial<T>

  /**
   * Sets reference to current value of target
   */
  reset: () => void

  /**
   * Returns true if object has changed from references
   */
  hasChanged: boolean
}

/**
 * Computes changes on plain object (a.k.a not reactive)
 */
export class PlainRecorder<T extends Record<string, unknown>>
  implements Recorder<T>
{
  /**
   * @inheritDoc
   */
  reference: T

  target: T

  constructor(target: T) {
    this.target = target
    this.reference = cloneDeep<T>(target)
  }

  /**
   * @inheritDoc
   */
  get changes(): Partial<T> {
    return getChanges(this.reference, this.target)
  }

  /**
   * @inheritDoc
   */
  reset(): void {
    this.reference = cloneDeep<T>(this.target)
  }

  /**
   * @inheritDoc
   */
  get hasChanged(): boolean {
    return Object.keys(this.changes).length > 0
  }
}

/**
 * Computes changes between target and reference, return changes
 * Updates it, if passed as parameter
 */
export function getChanges<T extends Record<string, unknown>>(
  reference: T,
  newValue: T
): Partial<T> {
  const changes: Partial<T> = {}
  // Update changed values
  Object.entries(newValue).forEach(([key, value]) => {
    if (!isEqual(reference[key], value)) {
      Object.assign(changes, cloneDeep({ [key]: value }))
    }
  })
  return changes
}
