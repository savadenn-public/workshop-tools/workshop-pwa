import { computed, isRef, ref, unref, watch } from 'vue'
import { cloneDeep } from 'lodash'
import { Recorder, getChanges } from '@/components/form/PlainRecorder'
import { WatchSource } from '@vue/runtime-core'

function snapshot<T extends Record<string, unknown>>(
  target: WatchSource<T>
): T {
  if (isRef(target)) return cloneDeep<T>(unref(target))
  return cloneDeep<T>(target())
}

/**
 * Reactive changes on reactive or ref object
 */
export class ReactiveRecorder<T extends Record<string, unknown>> {
  /**
   * @inheritDoc
   *
   * This is NOT reactive
   */
  reference: T

  /**
   * Reactive result of recoder
   */
  result = computed<Recorder<T>>(() => {
    return {
      changes: this.refChanges.value,
      hasChanged: Object.keys(this.refChanges.value).length > 0,
      reference: this.reference,
      reset: () => this.reset(),
    }
  })

  target: WatchSource<T>

  /**
   * @inheritDoc
   */
  refChanges = ref<Partial<T>>({})

  /**
   * @param target Function pointing to target (Same as Vue.watch)
   */
  constructor(target: WatchSource<T>) {
    this.target = target
    this.reference = snapshot(target)
    watch(
      target,
      (newValue) => {
        return (this.refChanges.value = getChanges(
          this.reference,
          cloneDeep(newValue)
        ))
      },
      { deep: true }
    )
  }

  /**
   * @inheritDoc
   */
  reset(): void {
    this.reference = snapshot(this.target)
    this.refChanges.value = {}
  }
}
