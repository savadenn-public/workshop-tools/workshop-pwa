import { getChanges, PlainRecorder } from '@/components/form/PlainRecorder'

type Test = { foo: string; newKey?: string }

describe('PlainRecorder', () => {
  let source: Test = { foo: 'bar' }
  let record = new PlainRecorder(source)
  beforeEach(() => {
    source = { foo: 'bar' }
    record = new PlainRecorder(source)
  })

  it('tracks change on plain object', async () => {
    source.foo = 'bar2'
    expect(record.hasChanged).toBe(true)
    expect(record.changes).toEqual({ foo: 'bar2' })
    expect(record.reference).toEqual({ foo: 'bar' })
  })

  it('handles rollback', () => {
    source.foo = 'bar2'
    source.foo = 'bar'
    expect(record.hasChanged).toBe(false)
    expect(record.changes).toEqual({})
    expect(record.reference).toEqual({ foo: 'bar' })
  })

  it('handles new key', () => {
    source.newKey = 'foo'
    expect(record.hasChanged).toBe(true)
    expect(record.changes).toEqual({ newKey: 'foo' })
    expect(record.reference).toEqual({ foo: 'bar' })
  })

  it('PlainRecorder.reset', async () => {
    source.foo = 'newReference'
    record.reset()
    expect(record.reference).toEqual({ foo: 'newReference' })
    expect(record.hasChanged).toBe(false)
    expect(record.changes).toEqual({})
    // Going to initial value is detecting as change as reference has been reset
    source.foo = 'bar'
    expect(record.hasChanged).toBe(true)
    expect(record.changes).toEqual({ foo: 'bar' })
  })
})

type AnyObject = Record<string, unknown>
type TestCase = {
  a: AnyObject
  b: AnyObject
  diff: Partial<AnyObject>
}

const tests: Record<string, TestCase> = {
  'simple object': {
    a: { foo: 'bar' },
    b: { foo: 'bar2' },
    diff: { foo: 'bar2' },
  },
  scalars: {
    a: { array: [true, 'foo', 0], bool: true, string: 'foo', number: 0 },
    // order matters in array
    b: { array: [0, 'foo', true], bool: false, string: 'bar', number: 5 },
    diff: { array: [0, 'foo', true], bool: false, string: 'bar', number: 5 },
  },
  complexObject: {
    a: {
      isEqual: { same: 'foobar' },
      is: { equal: 'foobar', notEqual: 'foobar' },
    },
    b: {
      isEqual: { same: 'foobar' },
      is: { equal: 'foobar', notEqual: 'a_change' },
    },
    diff: { is: { equal: 'foobar', notEqual: 'a_change' } },
  },
}

describe('getChanges', () => {
  for (const [message, testCase] of Object.entries(tests))
    it(message, () => {
      expect(getChanges(testCase.a, testCase.b)).toEqual(testCase.diff)
    })
})
