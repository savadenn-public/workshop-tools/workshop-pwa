import { createApp } from 'vue'
export * from './validation'
export * from './displayDate'
import Description from './Description.vue'
import ErrorWrapper from './ErrorWrapper.vue'
import Title from './Title.vue'
import Duration from './Duration.vue'
import InputText from './InputText.vue'

/**
 * Registers all form components globally
 */
export function registerFormComponents(
  app: ReturnType<typeof createApp>
): void {
  app.component('ErrorWrapper', ErrorWrapper)
  app.component('Description', Description)
  app.component('Title', Title)
  app.component('Duration', Duration)
  app.component('InputText', InputText)
}
