import { reactive } from 'vue'
import { UnwrapNestedRefs } from '@vue/reactivity'

export type Violation = {
  code: string | null
  message: string
  propertyPath: string
}

/**
 * An error relevant to a special field
 */
export class FieldError {
  code: string | null
  message: string

  constructor(params: { message: string; code?: string | null }) {
    this.message = params.message
    this.code = params.code ?? null
  }

  /**
   * @inheritDoc
   */
  toString(): string {
    return this.message
  }
}

export type FieldErrors = FieldError[]
export type FieldName = string
export type Errors = Record<FieldName, FieldErrors>

export type UseViolation = {
  /**
   * Reactive errors
   */
  errors: UnwrapNestedRefs<Errors>

  /**
   * Clear error messages of given field list
   */
  clearErrors: () => void

  /**
   * Parse the given violations into errors
   */
  parseViolations: (violations: Violation[]) => void
}

/**
 * Handle fields validation
 */
export function useFormValidation(): UseViolation {
  /**
   * @inheritDoc
   */
  const errors = reactive<Errors>({})

  /**
   * @inheritDoc
   */
  function clearErrors(): void {
    Object.keys(errors).forEach((key) => delete errors[key])
  }

  /**
   * @inheritDoc
   */
  function parseViolations(violationList: Violation[]): void {
    clearErrors()

    for (const violation of violationList) {
      if (!errors[violation.propertyPath]) {
        errors[violation.propertyPath] = []
      }

      errors[violation.propertyPath].push(new FieldError(violation))
    }
  }

  return {
    errors,
    clearErrors,
    parseViolations,
  }
}
