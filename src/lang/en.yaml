_name: English
action:
  add: Add
  archive: Archive
  cancel: Cancel
  clear: Clear
  clone: Duplicate
  create: Create
  delete: Delete
  edit: Edit
  reduce: Reduce
  reopen: Re-open
  save: Save
  search: Search
  show: Show
  update: Update
component:
  assigneePicker:
    assignTo: Assign to {workforce}
    noOne: Without assignation
    unassign: Remove assignee
  avatar:
    body: Body
    clothing: Clothing
    create: Create your avatar
    ear:
      big: big
      small: small
    earSize: Ears
    earring:
      big: big
      none: none
      small: small
    earringType: Earrings
    eye:
      circle: round
      oval: oval
      shadow: shadowed
      smile: smiling
    eyeType: Eyes
    eyebrow:
      down: down
      eyelashesDown: eyelashed down
      eyelashesUp: eyelashed up
      up: up
    eyebrowType: Eyebrows
    glasses:
      none: none
      pilote: pilote
      round: round
      square: squared
    glassesType: Glasses
    hair:
      bob: bob
      crew: crew
      feathered: feathered
      mohawk: mohawk
      none: none
      pixie: pixie
      thick: thick
    hairType: Haircut
    hat:
      beanie: beanie
      none: none
      top: top hat
      turban: turban
    hatType: Hat
    modify: Modify your avatar
    mouth:
      laugh: laugh
      nervous: nervous
      pucker: pucker
      smile: smile
      smirk: happy
      surprised: surprised
    mouthType: Mouth
    nose:
      curved: curved
      pointed: pointed
      round: round
    noseType: Nose
    randomize: Random avatar
    shirt:
      crew: crew
      polo: polo
      tee: tee-shirt
    shirtType: Shirt
    skin: Skin
  cloneBusinessCase:
    cloned: Business case duplicated
    title: Duplicate business case "{name}"
  confirm_deletion:
    confirm: Do you confirm?
    error: '{type} "{title}" could not be deleted.'
    header: Deletion confirmation
    message: 'You are about to delete'
    success: '{type} "{title}" has been deleted.'
  dueDatePicker:
    remove: Remove due date
    removed: Due date removed
    set: Set due date
    update: Update due date
    updated: Due date updated
  duration:
    label: Estimated duration
  legalNotice:
    cookies:
      content: 'This site use no cookies.'
      title: Cookie policy
    editor: Site editor
    hosting: Hosting
    madeBy: Made by
    privacy:
      content: |
        The site respects the privacy of the Internet user and strictly complies with the laws in force on the 
        protection of privacy and individual freedoms. No personal information is collected without your knowledge.
        No personal information is disclosed to third parties. Emails, electronic addresses or other nominative
        information sent to this site are not used and are only kept for the time necessary for their processing.
        In accordance with the provisions of law n ° 78-17 of January 6, 1978 relating to data processing, files and 
        freedoms, Internet users have the right to access, modify, rectify and delete data that concern. This right is 
        exercised by post, providing proof of identity, to the company address written above.
      title: Privacy policy
    publishingDirector:
      content: Sébastien Meudec as CEO of Savadenn
      title: Publication director
    title: Legal notice
    trademark:
      content: |
        The website is owned and operated by Savadenn. All the contents including, without limitation, images, graphics,
        texts, videos, logos, and icons are the exclusive property of Savadenn with the exception of trademarks, logos
        or content belonging to other partner companies or authors. The reproduction, representation, transfer,
        distribution, or registration of all or part of these elements is strictly prohibited without the express
        authorization of Savadenn.
      title: Trademark
    useTerm:
      content: 'This website is restricted to authorized user only. '
      title: 'Terms & Conditions of Usage'
  login:
    apiUnavailable: There is an error, verify your internet connection and contact support.
    email: Email address
    invalidCredentials: Provided credentials are invalid. Please, verify them.
    password: Password
    passwordReqs:
      optional:
        else: Something else
        lower: A lower characters
        number: A digit
        title: Recommended
        upper: A upper character
      required:
        length: At least {length} characters
        title: Required
    recover:
      info: If not, you may not have access to this service. Please check also your spam folder.
      start: Forgot your password?
      submit: Send my recovery link
      success: You should receive a mail with your recovery link soon.
    resetPassword:
      error: Provided token is invalid
      info: Verify that you copied the link correctly or generate another one.
      password: New password
      submit: Change your password
    submit: Log In
  menu:
    logout: Log out
  sidebar:
    minimize: Collapse menu
    open: Open menu
label:
  clear_filters: Clear
  filter:
    after_date: Date is after
    before_date: Date is before
  quick_search: Quick search
page:
  administration:
    title: Administration
  apiKeys:
    copyToken: Please, copy the following token has you will not be able to access it later.
    createSuccess: New API key created.
    title: API Keys
  business_case:
    task_list: Tasks ({count})
    title: Business case
  business_cases:
    no_data: No business case
    order:
      ASC: Ascending deadline
      DESC: Descending deadline
    search_by:
      code: Search by code
      name: Search by name
    title: Business cases
  home:
    title: Workshop Planner
  machines:
    title: Machines
  settings:
    email:
      submit: Change e-mail
      title: What is your email address ?
    error: Error
    lang:
      label: Language
    password:
      current: Current password
      new: New password
      submit: Change password
      successDetail: Password
      title: Security
    profile:
      email: What is your email address ?
      name: How should we call you ?
      success:
        detail: Profile
      title: Your profile
    success: Updated
    theme:
      label: Themes
      themeDark: Dark
      themeLight: Light
    timeZone:
      label: Display time as
    title: Settings
  task:
    controlPoints: Control points
    showInPlanning: Show in planning
    title: Task
  teams:
    title: Teams
  users:
    create:
      submit: Create
      title: Create user
    title: Users list
    update:
      submit: Update
      title: Update user
  weekPlanning:
    backlog:
      title: Backlog
    failLiveReload:
      detail: You will not see task changes in real time. Try to reload the page.
      title: Error
    filters:
      blockWidth: Block width
      byCase: Row per cases
      byResource: Row per resources
      days: How many days to show ?
      granularity: Granularity
      granularityOptions:
        0_25: Quarter
        0_5: Half-hour
        day: 'Day | {n} days'
        halfDay: Half-day
        hour: 'Hour | {n} hours'
        week: Week
      manage: Manage saved dashboard
      next: Next day | Next {n} days
      previous: Previous day | Previous {n} days
      today: Today
      tune: Configure planning
      workforces:
        help: If none is selected, all workforces will be displayed
        label: What workforce to display?
        placeholder: All
    notAssigned: Not assigned
    ruler:
      week: Week
    title: Week board
resource:
  apiKey:
    _name: API key | API keys
    create: Create an API key
    usage: Usage
  business_case:
    _name: business case | business cases
    archived: Archived business case | Archived business cases
    code: Code
    create: Create business case
    created: Created
    createdAt: Created
    description: Description
    dueDate: Due date
    name: Name
    opened: Opened business case | Opened business cases
    title: Business Case
  controlPoint:
    _name: Control Point | Control Points
    description: Description
  controlPointValidation:
    add: Additional validation
    approvedBy: validated by
    create: Validate
    remove: Remove validation
  machine:
    create: Create machine
  planningSettings:
    name: Name
  task:
    _name: task | tasks
    close: End task
    closed: Ended task
    create: Create task
    createdAt: Created
    description: Description
    duration: Estimated duration
    durationMax: Maximal estimated duration
    durationMin: Minimal estimated duration
    endDate: End Date
    estimatedDuration: '{min} to {max}'
    estimatedEndDate: Estimated end date
    loadMore: Load more tasks
    notAssigned: Not assigned
    overdue: Ends after due date
    reopen: Re-open task
    search: Search tasks
    spentTime: Spent time
    startDate: Start date
    title: Task
    undefinedDate: Undefined
  team:
    create: Create team
  undefined: Empty
  untitled: Untitled
  user:
    _name: user | users
    active: Active account
    create: Create user
    created:
      detail: An invitation link has been sent by email to the user.
      summary: User created
    email: E-mail address
    locked: Account locked since {date}
    name: Name
    role_admin: Administrator
    roles: Role
    status: Account status
    unknown: Unknown user
    updated:
      detail: User updated
      summary: Saved
toast:
  error:
    change_email:
      detail: Cannot change your e-mail address.
    title: Error
  success:
    business_case_created: Business case created
    change_email:
      confirmed:
        detail: You e-mail address has been updated.
        summary: E-mail updated
      saved:
        detail: You will receive a confirmation link by e-mail.
        summary: Saved
    deleted: Deleted
units:
  hour: '{n} hour | {n} hour | {n} hours'
