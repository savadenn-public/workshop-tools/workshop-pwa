import { usePrimeVue } from 'primevue/config'

export const fr: ReturnType<typeof usePrimeVue>['config']['locale'] = {
  startsWith: 'Commence par',
  contains: 'Contient',
  notContains: 'Ne contient pas',
  endWith: 'Fini par',
  equals: 'Égal',
  notEquals: 'Différent',
  noFilter: 'Sans filter',
  lt: 'Inférieur à',
  lte: 'Inférieur ou égale à',
  gt: 'Supérieur à',
  gte: 'Supérieur ou égale à',
  dateIs: 'La date est',
  dateIsNot: 'La date n’est pas',
  dateBefore: 'La date est avant',
  dateAfter: 'La date est après',
  clear: 'Vider',
  apply: 'Appliquer',
  matchAll: 'Tous',
  matchAny: 'Aucun',
  addRule: 'Ajouter une règle',
  removeRule: 'Retirer une règle',
  accept: 'Oui',
  reject: 'Non',
  choose: 'Choisir',
  upload: 'Téléverser',
  cancel: 'Annuler',
  monthNames: [
    'Janvier',
    'Février',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Août',
    'Septembre',
    'Octobre',
    'Novembre',
    'Décembre',
  ],
  monthNamesShort: [
    'Jan',
    'Fév',
    'Mar',
    'Avr',
    'Mai',
    'Jun',
    'Jul',
    'Aoû',
    'Sep',
    'Oct',
    'Nov',
    'Déc',
  ],
  dayNames: [
    'Dimanche',
    'Lundi',
    'Mardi',
    'Mercredi',
    'Jeudi',
    'Vendredi',
    'Samedi',
  ],
  dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
  dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
  weekHeader: 'Sem',
  today: 'Aujourd’hui',
  firstDayOfWeek: 1,
  dateFormat: 'dd/mm/yy',
  weak: 'Faible',
  medium: 'Moyen',
  strong: 'Fort',
  passwordPrompt: 'Entrer un mot de passe',
  emptyFilterMessage: 'Aucun résultat trouvé',
  emptyMessage: 'Aucune option disponible',
}
