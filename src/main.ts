import { createApp } from 'vue'
import App from '@/App.vue'
import AppError from '@/AppError.vue'
import * as Plugins from '@/plugins'
import { initialize } from '@/states'

import { registerPrimeVue } from '@/PrimeVue'
import { registerFormComponents } from '@/components/form'

import 'normalize.css'
import 'primevue/resources/primevue.min.css'
import '@mdi/font/css/materialdesignicons.min.css'
import 'primeicons/primeicons.css'
import 'typeface-poppins'
import '@/style/main.scss'

import { registerResourceComponents } from '@/components/resources'

// Purge service-worker if any
navigator.serviceWorker
  ?.getRegistrations()
  ?.then((registrations) =>
    registrations.forEach((registration) => registration.unregister())
  )

async function main() {
  try {
    await initialize()
    const app = createApp(App)
    app.use(Plugins.Router)
    registerPrimeVue(app)
    registerFormComponents(app)
    registerResourceComponents(app)
    app.use(Plugins.i18n)
    app.mount('body')
  } catch (error) {
    createApp(AppError, { error }).mount('body')
  }
}

// noinspection JSIgnoredPromiseFromCall
main()
