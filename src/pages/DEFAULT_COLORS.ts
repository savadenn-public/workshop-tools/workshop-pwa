export const DEFAULT_COLORS = [
  { primary: '#FA6800', secondary: '#FFDCC3' },
  { primary: '#3F7FFF', secondary: '#A9C5FD' },
  { primary: '#FF5A80', secondary: '#FFCDD8' },
  { primary: '#37A637', secondary: '#BCE0BC' },
  { primary: '#FFD036', secondary: '#F9E6B6' },
  { primary: '#CC68FE', secondary: '#F1D4FF' },
  { primary: '#00ABA9', secondary: '#B9E5E5' },
  { primary: '#F472D0', secondary: '#EDC7E3' },
  { primary: '#A4C400', secondary: '#E0E8B8' },
  { primary: '#1BA1E2', secondary: '#AEE4FB' },
]
