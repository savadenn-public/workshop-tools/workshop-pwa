import { computed, Ref, ref, unref, watchEffect } from 'vue'
import { User, userClient, ROLE_ADMIN, ROLE_USER } from '@/api'
import { addMessage } from '@/states'
import { AxiosError } from 'axios'
import { useFormValidation } from '@/components/form'
import { i18n } from '@/types'
import { cloneDeep } from 'lodash'

// From https://stackoverflow.com/a/46181
const VALID_EMAIL_REGEX =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

export const DEFAULT_USER: Partial<User> = { roles: [ROLE_USER] }

export type CurrentUser = User | typeof DEFAULT_USER | null

export function useUser(user: Ref<CurrentUser>, t: i18n.t) {
  const { clearErrors, parseViolations, errors } = useFormValidation()

  /**
   * Dataset with current user data
   */
  const userModel = ref<Partial<User>>({})

  watchEffect(() => {
    if (user.value) {
      userModel.value = cloneDeep(user.value)
    } else reset()
  })

  function reset() {
    userModel.value = { roles: [ROLE_USER] }
  }

  /**
   * Whether required data is valid
   */
  const isValid = computed<boolean>(() =>
    VALID_EMAIL_REGEX.test(String(userModel.value.email).toLowerCase())
  )

  const submitting = ref<boolean>(false)

  /**
   * Creating a user or updating an existing one
   */
  const mode = computed(() => (!userModel.value.id ? 'create' : 'update'))

  /**
   * Whether the user account is locked
   */
  const isLocked = computed<boolean>({
    get: () => Boolean(userModel?.value?.lockDate),
    set: (value: boolean) => {
      userModel.value.lockDate = null

      if (value) {
        userModel.value.lockDate = new Date()
      }
    },
  })

  /**
   * True if user is admin
   */
  const isAdmin = computed<boolean>({
    get: () => userModel?.value?.roles?.includes(ROLE_ADMIN) || false,
    set: (value: boolean) => {
      userModel.value.roles = [ROLE_USER]

      if (value) {
        userModel.value.roles = [ROLE_ADMIN]
      }
    },
  })

  /**
   * Submit data to create a new user or update existing one
   * Close dialog if succeed, display errors otherwise
   */
  const submit = async (): Promise<User | void> => {
    clearErrors()
    if (!isValid.value) return

    try {
      submitting.value = true
      const user = userModel.value.id
        ? await userClient.patch(
            userModel.value.id as User['id'],
            unref(userModel)
          )
        : await userClient.create(unref(userModel))

      addMessage({
        severity: 'success',
        summary: t(`resource.user.${mode.value}d.summary`),
        detail: t(`resource.user.${mode.value}d.detail`),
        life: 3000,
      })

      return user
    } catch (e) {
      if ((e as AxiosError)?.response?.status === 422) {
        parseViolations((e as AxiosError).response?.data.violations)
        return
      }
      console.error(e)
    } finally {
      submitting.value = false
    }
  }

  return {
    userModel,
    isValid,
    isAdmin,
    isLocked,
    submit,
    errors,
    reset,
    mode,
    submitting,
  }
}
