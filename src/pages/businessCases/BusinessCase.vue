<template>
  <main
    :class="{
      BusinessCase: true,
      'BusinessCase--edit': editMode,
    }"
  >
    <ResourceHeader
      v-model:edit-mode="editMode"
      resource-type="business_case"
      :resource="businessCase"
      :loading="false"
      :items="items"
      :main-action="mainAction"
      :no-modify-button="!!businessCase.archiveDate"
    />

    <div class="BusinessCase__TitleHead" :style="colorStyle">
      <div class="BusinessCase__TitleCode">
        <!-- color -->
        <ColorChooser
          v-if="editMode && !businessCaseLoading"
          v-model="businessCase.color"
          allow-custom
          allow-remove
          :preferred="DEFAULT_COLORS.map((color) => color.primary)"
          class="BusinessCase__Color"
          @remove-color="businessCase.color = null"
        />
        <span
          v-if="!editMode && businessCase.color"
          class="BusinessCase__Color--display"
        />
        <!-- Name -->
        <Title
          v-model="businessCase.name"
          :edit="editMode"
          :errors="businessCaseErrors.name"
          :label="t('resource.business_case.name')"
          :loading="businessCaseLoading"
          class="BusinessCase__Name"
        />

        <!-- Code -->
        <InputText
          v-model="businessCase.code"
          :edit="editMode"
          :errors="businessCaseErrors.code"
          :label="t('resource.business_case.code')"
          :loading="businessCaseLoading"
          class="BusinessCase__Code"
          :class="{ 'BusinessCase__Code--loading': businessCaseLoading }"
        />
      </div>

      <!-- Due date -->
      <div
        v-if="!businessCaseLoading && businessCase.dueDate"
        class="BusinessCase__DueDate"
        :class="{ 'BusinessCase__DueDate--passed': isDueDatePassed() }"
        :title="t('resource.business_case.dueDate')"
      >
        <i class="mdi mdi-calendar"></i>
        {{ moment(businessCase.dueDate).format('LL') }}
      </div>
      <PSkeleton
        v-if="businessCaseLoading"
        class="BusinessCase__DueDate BusinessCase__DueDate--skeleton"
      />
    </div>

    <!-- Description -->
    <Description
      v-model="businessCase.description"
      :edit="editMode"
      :errors="businessCaseErrors.description"
      :loading="businessCaseLoading"
      class="BusinessCase__Description"
    />

    <!-- Action buttons -->
    <PButton
      v-if="editMode"
      class="BusinessCase__Button p-button p-button-secondary"
      :label="t('action.cancel')"
      icon="pi pi-times"
      @click="cancel"
    />
    <PButton
      v-if="editMode"
      class="BusinessCase__Button p-button"
      :label="t('action.update')"
      icon="pi pi-check"
      @click="submit"
    />

    <section>
      <TaskList
        v-if="!editMode"
        v-model:focused-task-updated="focusedTaskUpdated"
        v-model:deleted-task="deletedTask"
        :uuid="businessCase.id"
        @update:task-focus="taskFocus = $event"
      />
    </section>
  </main>

  <ConfirmDeleteDialog
    v-model:visible="displayConfirmDelete"
    :uuid="props.uuid"
    resource-key="business_case"
    :resource-name="businessCase.name"
    :redirect-url="{ ...ROUTES.BUSINESS_CASES }"
  />

  <CopyBusinessCaseDialog
    v-model:visible="displayCloneDialog"
    :business-case="businessCase"
  />

  <!-- Include special action on resource -->
  <component
    :is="componentSpecificAction"
    v-if="componentSpecificAction"
    :business-case="businessCase"
    @success="successSpecificAction"
    @cancel="cancelSpecificAction"
  />

  <TaskSideBar
    v-if="taskFocus"
    :uuid="taskFocus"
    @delete:focused-task="onTaskDelete"
    @update:focused-task="focusedTaskUpdated = $event"
  />
</template>
<script lang="ts" setup>
import { useRoute, useRouter } from 'vue-router'
import { computed, ref, toRefs } from 'vue'
import { useI18n } from 'vue-i18n'
import moment from 'moment-timezone'
import TaskList from '@/pages/businessCases/partial/TaskList.vue'
import ResourceHeader from '@/components/resources/ResourceHeader.vue'
import { useActions } from '@/pages/businessCases/useActions'
import ConfirmDeleteDialog from '@/components/resources/ConfirmDeleteDialog.vue'
import { ROUTES } from '@/plugins'
import { useBusinessCase } from '@/pages/businessCases/useBusinessCase'
import type { BusinessCase, Task } from '@/api'
import TaskSideBar from '@/pages/businessCases/partial/TaskSideBar.vue'
import ColorChooser from '@/components/ColorChooser.vue'
import { DEFAULT_COLORS } from '@/pages/DEFAULT_COLORS'

const props = defineProps<{
  uuid: string
}>()
const { t } = useI18n()
const { uuid } = toRefs(props)
const editMode = ref<boolean>(false)
const route = useRoute()
const router = useRouter()

const taskFocus = ref<Task['id'] | null>()
const focusedTaskUpdated = ref<Task['id'] | null>(null)
const deletedTask = ref<Task['id'] | null>(null)
const colorStyle = computed(() =>
  businessCase.value?.color
    ? `--business-case-color: ${businessCase.value?.color};`
    : ''
)

function onBusinessCaseNotFound() {
  router.push(ROUTES.BUSINESS_CASES)
}

const {
  businessCase,
  patchBusinessCase,
  rollbackBusinessCase,
  businessCaseErrors,
  businessCaseLoading,
} = useBusinessCase(uuid, onBusinessCaseNotFound)

const {
  displayConfirmDelete,
  displayCloneDialog,
  items,
  componentSpecificAction,
  mainAction,
} = useActions(businessCase, patchBusinessCase, t)

function cancelSpecificAction() {
  componentSpecificAction.value = null
}

function successSpecificAction(changed: BusinessCase) {
  componentSpecificAction.value = null
  businessCase.value = changed
}

/**
 * Submit businessCase changes
 */
async function submit() {
  if (await patchBusinessCase()) editMode.value = false
}

/**
 * Whether business case due date is passed
 */
function isDueDatePassed(): boolean {
  return Boolean(
    businessCase.value.dueDate && businessCase.value.dueDate < new Date()
  )
}

/**
 * Removes focus when a task is deleted from sidebar
 * and allows its deletion from the task list
 */
function onTaskDelete(id: Task['id']) {
  deletedTask.value = id
  taskFocus.value = null
}

/**
 * Exit edit mode
 */
function cancel() {
  rollbackBusinessCase()
  editMode.value = false
}
</script>

<style lang="scss" scoped>
@use 'src/style/variables' as v;

.BusinessCase {
  $root: &;

  padding: v.$spacing;

  &__TitleHead {
    display: flex;
    flex-direction: column;
    border-bottom: solid 1px var(--surface-d);
  }

  &__TitleCode {
    display: flex;
    align-items: baseline;
    margin: v.$spacing 0;
    column-gap: v.$spacing-2;

    &:deep(#{$root}__Code--loading::before) {
      display: none;
    }
    &:deep(#{$root}__Code) {
      margin: 0;
      font-weight: bold;
      font-style: italic;
      font-size: 2rem;

      &::before {
        content: '#';
      }
    }
  }

  &--edit {
    #{$root}__TitleCode {
      align-items: end;
    }
  }

  &:deep(#{$root}__Name) {
    margin: 0;
  }

  &__DueDate {
    margin-bottom: v.$spacing-2;
    font-size: 0.85rem;

    &--skeleton {
      flex-grow: 0;
      max-width: 200px;
    }

    &--passed {
      color: var(--pink-700);
    }
  }

  &:deep(#{$root}__Description) {
    margin: 1rem 0;
  }

  &__Button {
    margin-right: 0.5rem;
    margin-bottom: v.$spacing-2;
  }

  &__Color {
    padding-bottom: 0.8rem;

    &--display {
      display: block;
      width: 2rem;
      height: 2rem;
      border-radius: v.$borderRadius;
      background-color: var(--business-case-color);
      font-size: 2rem;
    }
  }
}
</style>
