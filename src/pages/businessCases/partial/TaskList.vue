<template>
  <PToolbar class="TaskList__Toolbar">
    <template #start>
      <h2 class="TaskList__Title">
        {{ t('page.business_case.task_list', { count: taskCount }) }}
      </h2>
    </template>
    <template #end>
      <PButton
        :label="t('action.add')"
        icon="pi pi-plus"
        @click="createMode = true"
      />
    </template>
  </PToolbar>

  <!-- Form to create a new task in place -->
  <form v-show="createMode" class="TaskForm" @submit.prevent="createTask">
    <label class="TaskForm__Title">
      <span class="TaskForm__Label">{{ t('resource.task.title') }}</span>
      <PInputText
        v-model="newTask.title"
        type="text"
        :placeholder="t('resource.task.title')"
        :class="['TaskForm__TitleInput', errors.title ? 'p-invalid' : '']"
      />
    </label>
    <ErrorWrapper :errors="errors.title" />

    <!-- Cancel button -->
    <PButton class="TaskForm__Button p-button-text" @click="cancelCreate">
      {{ t('action.cancel') }}
    </PButton>

    <!-- Submit button -->
    <PButton
      class="TaskForm__Button p-button-primary"
      type="submit"
      :disabled="!newTask.title"
    >
      {{ t('action.create') }}
    </PButton>
  </form>

  <!-- Task list -->
  <ul v-if="taskCount" class="Tasks">
    <TaskItem
      v-for="task in tasks"
      :key="task.id"
      :task="task"
      :focused-task="focusedTask"
      @update:task-focus="onFocusChange"
    />
  </ul>
</template>
<script lang="ts" setup>
import { BusinessCase, DEFAULT_DURATION, Task, taskClient } from '@/api'
import {
  onMounted,
  readonly,
  ref,
  toRefs,
  unref,
  watch,
  watchEffect,
} from 'vue'
import { useI18n } from 'vue-i18n'
import { useFormValidation } from '@/components/form'
import ErrorWrapper from '@/components/form/ErrorWrapper.vue'
import TaskItem from '@/pages/businessCases/partial/TaskItem.vue'
import { AxiosError } from 'axios'

const props = defineProps<{
  uuid: BusinessCase['id']
  /**
   * ID of the task that has been updated from sidebar
   */
  focusedTaskUpdated: Task['id'] | null
  /**
   * ID of the task that has been deleted from sidebar
   */
  deletedTask: Task['id'] | null
}>()

const emits = defineEmits<{
  /**
   * Fires when the focused task changes
   */
  (e: 'update:task-focus', value: Task['id']): void
  /**
   * Fires when the focused task has been updated from task sidebar
   * and reloaded by this component
   */
  (e: 'update:focusedTaskUpdated', value: null): void
  /**
   * Fires when a task has been deleted from task array
   */
  (e: 'update:deletedTask', value: null): void
}>()
const { uuid } = toRefs(props)
const { errors, clearErrors, parseViolations } = useFormValidation()
const { t } = useI18n()
const tasks = ref<Task[]>([])
const taskCount = ref<number>(0)
const createMode = ref<boolean>(false)

const DEFAULT_TASK = readonly<Partial<Task>>({
  businessCase: uuid.value,
  durationMin: DEFAULT_DURATION,
  durationMax: 2 * DEFAULT_DURATION,
})

const newTask = ref<Partial<Task>>({ ...DEFAULT_TASK })

const focusedTask = ref<Task['id']>()

function onFocusChange(id: Task['id']) {
  focusedTask.value = id
  emits('update:task-focus', id)
}

/**
 * Reload the focused task once it has been updated
 */
watchEffect(async () => {
  if (props.focusedTaskUpdated) {
    const idx = taskIndexFromId(props.focusedTaskUpdated)
    if (idx > -1) {
      tasks.value[idx] = await taskClient.getItem(props.focusedTaskUpdated)
      emits('update:focusedTaskUpdated', null)
    }
  }
})

/**
 * Deletes task from task array if it has been deleted from sidebar
 */
watchEffect(() => {
  if (props.deletedTask) {
    const idx = taskIndexFromId(props.deletedTask)
    if (idx > -1) {
      tasks.value.splice(idx, 1)
      emits('update:deletedTask', null)
      taskCount.value--
    }
  }
})

/**
 * Return index of the given task in task array, -1 if the task has not been found
 */
function taskIndexFromId(id: Task['id']) {
  return tasks.value.findIndex((t: Task) => t['id'] === id)
}

/**
 * Load all tasks of the business case
 */
const getAllTasks = async () => {
  tasks.value = []
  let result = await taskClient.getCollection({
    businessCase: uuid.value,
  })
  taskCount.value = result.total

  result.resources.forEach((task) => tasks.value.push(task))
  while (result.next) {
    result = await result.next()
    result.resources.forEach((task) => tasks.value.push(task))
  }

  tasks.value.sort(
    (t1, t2) => (t1.startDate?.getTime() || 0) - (t2.startDate?.getTime() || 0)
  )
}

/**
 * Hide form, clear errors & reset model value
 */
const cancelCreate = () => {
  clearErrors()
  newTask.value = { ...DEFAULT_TASK }
  createMode.value = false
}

/**
 * Create new task
 */
const createTask = async () => {
  clearErrors()
  try {
    const task = await taskClient.create(unref(newTask))
    tasks.value.unshift(task)
    newTask.value = { ...DEFAULT_TASK }
    taskCount.value++
  } catch (e) {
    if ((e as AxiosError)?.response?.status === 422) {
      parseViolations((e as AxiosError)?.response?.data.violations)
      return
    }
    console.error(e)
  }
}

watch(uuid, getAllTasks)

onMounted(getAllTasks)
</script>
<style lang="scss" scoped>
@use 'src/style/variables' as v;

.TaskForm {
  display: flex;
  flex-wrap: wrap;
  gap: v.$spacing;
  padding: v.$spacing-2;
  border: solid 1px var(--surface-d);
  border-top: none;
  background: var(--surface-b);

  &__Label {
    display: block;
    margin: v.$spacing-2 0;
    font-weight: 500;
  }

  &__Title {
    flex: 1 1 100%;
  }

  &__TitleInput {
    width: 100%;
  }
}

.Tasks {
  margin: 0;
  padding: v.$spacing-2;
  border: solid 1px var(--surface-d);
  border-top: none;
  background: var(--surface-b);
  list-style: none;
}

.TaskList {
  &__Toolbar {
    gap: 1rem;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
  }

  &__Title {
    margin: 0;
  }
}
</style>
