import { computed, ref, Ref, shallowRef } from 'vue'
import { VueI18n } from 'vue-i18n'
import { ActionItem, MenuItem, translate } from '@/components/bars/MenuItem'
import type { BusinessCase, PartialResource } from '@/api'
import DueDatePicker from '@/pages/businessCases/partial/DueDatePicker.vue'

export type UseActions = {
  displayCloneDialog: Ref<boolean>
  displayConfirmDelete: Ref<boolean>
  items: Ref<Array<MenuItem | ActionItem>>
  mainAction: Ref<ActionItem | undefined>
  componentSpecificAction: Ref<null | typeof DueDatePicker>
}

export function useActions(
  businessCase: Ref<PartialResource<BusinessCase>>,
  patchBusinessCase: () => Promise<boolean>,
  t: VueI18n['t']
): UseActions {
  const componentSpecificAction = shallowRef<null | typeof DueDatePicker>(null)
  const displayCloneDialog = ref(false)
  const displayConfirmDelete = ref(false)

  const copyBusinessCase = {
    label: 'action.clone',
    icon: 'mdi mdi-content-copy',
    command: () => (displayCloneDialog.value = true),
    class: '',
  }

  const changeDueDate = {
    label: 'component.dueDatePicker.update',
    icon: 'mdi mdi-calendar',
    command: () => (componentSpecificAction.value = DueDatePicker),
    class: '',
  }

  const setDueDate = {
    label: 'component.dueDatePicker.set',
    icon: 'mdi mdi-calendar',
    command: () => (componentSpecificAction.value = DueDatePicker),
    class: '',
  }

  const archiveAction = {
    label: 'action.archive',
    icon: 'mdi mdi-archive-outline',
    command: () => {
      businessCase.value.archiveDate = new Date()
      patchBusinessCase()
    },
    class: '',
  }

  const reopenAction = {
    label: 'action.reopen',
    icon: 'mdi mdi-archive-off-outline',
    command: () => {
      businessCase.value.archiveDate = null
      patchBusinessCase()
    },
    class: '',
  }

  const deleteAction = {
    label: 'action.delete',
    icon: 'mdi mdi-delete-outline',
    command: () => (displayConfirmDelete.value = true),
    class: 'p-menuitem-danger',
  }

  return {
    displayCloneDialog,
    componentSpecificAction,
    displayConfirmDelete,
    items: computed(() => {
      const items: ActionItem[] = []

      items.push(copyBusinessCase)

      if (businessCase.value.archiveDate) {
        items.push(deleteAction)
      } else {
        if (!businessCase.value.dueDate) items.push(setDueDate)
        else items.push(changeDueDate)
        items.push(archiveAction)
      }

      return items.map((item) => translate(item, t))
    }),

    mainAction: computed(() => {
      if (businessCase.value.archiveDate) return reopenAction
      return undefined
    }),
  }
}
