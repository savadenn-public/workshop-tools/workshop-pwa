import { nextTick, Ref, ref, watchEffect } from 'vue'
import type { BusinessCase, PartialResource } from '@/api'
import { businessCaseClient } from '@/api'
import { ReactiveRecorder } from '@/components/form/ReactiveRecorder'
import { useFormValidation, UseViolation } from '@/components/form'
import { cloneDeep } from 'lodash'
import { AxiosError } from 'axios'
import { updateShortName } from '@/states/ShortNames'
import { bufferizeBusinessCase } from '@/states/resources'

export type UseBusinessCase = {
  businessCase: Ref<PartialResource<BusinessCase>>
  businessCaseRecorder: ReactiveRecorder<PartialResource<BusinessCase>>
  businessCaseLoading: Ref<boolean>
  patchBusinessCase: () => Promise<boolean>
  rollbackBusinessCase: () => void
  businessCaseErrors: UseViolation['errors']
}

export function useBusinessCase(
  businessCaseId: Ref<BusinessCase['id']>,
  onBusinessCaseNotFound: () => unknown
): UseBusinessCase {
  /**
   * Main business case
   */
  const businessCase = ref<PartialResource<BusinessCase>>({
    id: businessCaseId.value,
  })

  /**
   * Track changes on main business case
   */
  const businessCaseRecorder = new ReactiveRecorder<
    PartialResource<BusinessCase>
  >(businessCase)

  const { errors, clearErrors, parseViolations } = useFormValidation()

  const businessCaseLoading = ref<boolean>(true)

  /**
   * Get fresh businessCase from API
   */
  async function loadBusinessCase() {
    if (!businessCaseId.value) return

    try {
      businessCaseLoading.value = true
      businessCase.value = await businessCaseClient.getItem(
        businessCaseId.value
      )
      businessCaseRecorder.reset()
    } catch (e) {
      if ((e as AxiosError)?.response?.status === 404) {
        onBusinessCaseNotFound()
      } else {
        console.error(e)
      }
    } finally {
      businessCaseLoading.value = false
    }
  }

  /**
   * Refresh on ID change
   */
  watchEffect(loadBusinessCase)

  async function patchBusinessCase() {
    clearErrors()
    let success = false
    try {
      businessCaseLoading.value = true
      if (businessCase.value) await nextTick()
      if (businessCaseRecorder.result.value.hasChanged) {
        const businessCaseReturn = await businessCaseClient.patch(
          businessCaseId.value,
          businessCaseRecorder.result.value.changes
        )
        businessCase.value = businessCaseReturn
        bufferizeBusinessCase(businessCaseReturn)
        updateShortName(businessCase.value)
        businessCaseRecorder.reset()
      }
      success = true
    } catch (e) {
      if ((e as AxiosError)?.response?.status === 422) {
        parseViolations((e as AxiosError).response?.data.violations)
      } else {
        console.error(e)
      }
    }

    businessCaseLoading.value = false
    return success
  }

  function rollbackBusinessCase() {
    clearErrors()
    businessCase.value = cloneDeep(businessCaseRecorder.reference)
  }

  return {
    businessCase,
    businessCaseRecorder,
    businessCaseLoading,
    patchBusinessCase,
    rollbackBusinessCase,
    businessCaseErrors: errors,
  }
}
