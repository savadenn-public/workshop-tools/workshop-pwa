import { useActions } from '@/pages/tasks/useActions'
import { ref } from 'vue'
import { PartialResource, Task } from '@/api'
import { ActionItem } from '@/components/bars/MenuItem'

describe('useActions', function () {
  const patchTask = jest.fn()

  test('close task', function () {
    const t = jest.fn((key: string): string => key)
    const task = ref<PartialResource<Task>>({
      id: 'foo',
      startDate: new Date(),
    })

    const { items } = useActions(task, t, patchTask)
    expect(items.value.length).toEqual(2)

    const action = items.value.shift() as ActionItem
    expect(action.label).toBe('resource.task.close')
  })

  test('reopen task', function () {
    const t = jest.fn((key: string): string => key)
    const task = ref<PartialResource<Task>>({
      id: 'foo',
      startDate: new Date(),
      endDate: new Date(),
    })

    const { items } = useActions(task, t, patchTask)
    expect(items.value.length).toEqual(2)

    const action = items.value.shift() as ActionItem
    expect(action.label).toBe('resource.task.reopen')
  })
})
