import { nextTick, ref } from 'vue'
import MockAdapter from 'axios-mock-adapter'
import { useControlPoint } from '@/pages/tasks/useControlPoint'
import { Task } from '@/api'
import axios from 'axios'

const mockAxios = new MockAdapter(axios, { onNoMatch: 'throwException' })
jest.mock('@/api/Client')

describe('useControlPoint', function () {
  test('Change task', async function () {
    const task = ref<Task['id']>('foo')
    const { controls } = useControlPoint(task)

    expect(controls.value.length).toEqual(0)

    mockAxios.onGet('/api/control_points').reply(200, {
      'hydra:member': [
        {
          '@id': 1,
          task: 'bar',
          description: 'Dummy description',
          controlPointValidations: [],
          createdAt: '2022-01-01 10:25:36',
        },
      ],
      'hydra:totalItems': 1,
    })

    // Change task id expecting control points to be reloaded
    task.value = 'bar'

    // Wait for DOM to be updated
    for (let i = 0; i < 4; i++) await nextTick()

    expect(controls.value.length).toEqual(1)
  })

  test('Cancel creation', function () {
    const task = ref<Task['id']>('foo')
    const { cancelCreate, createMode, controlPoint } = useControlPoint(task)

    controlPoint.value.description = 'Dummy description'
    createMode.value = true

    cancelCreate()

    // Expect control point to be reset
    expect(controlPoint.value.description).toEqual('')

    // Expect createMode to be reset to FALSE
    expect(createMode.value).toEqual(false)
  })

  test('Add control point', async function () {
    const task = ref<Task['id']>('foo')
    const { controls, controlPoint, addControlPoint } = useControlPoint(task)

    controlPoint.value.description = 'Foo description'

    mockAxios
      .onPost('/api/control_points', {
        task: 'foo',
        description: 'Foo description',
      })
      .reply(201, {
        '@id': 2,
        task: 'foo',
        description: 'Foo description',
        controlPointValidations: [],
        createdAt: '2022-01-01 10:25:36',
      })

    await addControlPoint()

    // Expect created element to be appended to the list
    expect(controls.value.length).toEqual(1)

    // Expect model object to be reset
    expect(controlPoint.value.description).toEqual('')
  })

  test('deleteControlPoint', function () {
    const task = ref<Task['id']>('foo')
    const { deleteControlPoint, toBeDeleted, displayConfirmDelete } =
      useControlPoint(task)

    expect(toBeDeleted.value).toEqual('')
    expect(displayConfirmDelete.value).toEqual(false)

    deleteControlPoint('foobar')

    expect(toBeDeleted.value).toEqual('foobar')
    expect(displayConfirmDelete.value).toEqual(true)
  })

  test('onControlPointDelete', function () {
    const task = ref<Task['id']>('foo')
    const { onControlPointDelete, toBeDeleted, controls } =
      useControlPoint(task)

    controls.value.push({
      task: task.value,
      id: 'uid-1234',
      controlPointValidations: [],
      createdAt: new Date(),
      description: '',
    })

    toBeDeleted.value = 'uid-1234'

    onControlPointDelete()

    // Expect control point to be removed from list
    expect(controls.value.length).toEqual(0)
  })
})
