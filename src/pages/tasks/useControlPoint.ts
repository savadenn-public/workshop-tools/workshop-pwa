import { ref, Ref, UnwrapRef, watch } from 'vue'
import { Task } from '@/api'
import { ControlPoint, controlPointClient } from '@/api/resources/controlPoint'
import { Errors, useFormValidation } from '@/components/form'
import { AxiosError } from 'axios'

export type UseControlPoint = {
  controls: Ref<ControlPoint[]>
  controlPoint: Ref<Partial<ControlPoint>>
  loadControlPoints: () => void
  addControlPoint: () => void
  cancelCreate: () => void
  createMode: Ref<boolean>
  toBeDeleted: Ref<ControlPoint['id']>
  displayConfirmDelete: Ref<boolean>
  deleteControlPoint: (id: ControlPoint['id']) => void
  onControlPointDelete: () => void
  errors: UnwrapRef<Errors>
}

export function useControlPoint(taskId: Ref<Task['id']>): UseControlPoint {
  const controls = ref<ControlPoint[]>([])
  const createMode = ref<boolean>(false)
  const controlPoint = ref<Partial<ControlPoint>>({ task: taskId.value })

  const { errors, clearErrors, parseViolations } = useFormValidation()

  /**
   * Try to create a new ControlPoint & append it to the list if succeed
   */
  async function addControlPoint() {
    clearErrors()

    try {
      const resource = await controlPointClient.create(controlPoint.value)
      controls.value.push(resource)

      controlPoint.value.description = ''
    } catch (e) {
      if ((e as AxiosError)?.response?.status === 422) {
        parseViolations((e as AxiosError).response?.data.violations)
      } else {
        console.error(e)
      }
    }
  }

  /**
   * Cancel creation
   */
  function cancelCreate() {
    clearErrors()
    createMode.value = false
    controlPoint.value.description = ''
  }

  /**
   * Load control points of the current task
   */
  async function loadControlPoints() {
    let result = await controlPointClient.getCollection({
      task: taskId.value,
    })
    controls.value = result.resources

    while (result.next) {
      result = await result.next()
      controls.value.push(...result.resources)
    }
  }

  /**
   * Id of control point to delete
   */
  const toBeDeleted = ref<ControlPoint['id']>('')
  const displayConfirmDelete = ref<boolean>(false)

  /**
   * Display delete-confirmation modal
   */
  function deleteControlPoint(id: ControlPoint['id']) {
    toBeDeleted.value = id
    displayConfirmDelete.value = true
  }

  /**
   * Callback after a control point deletion: remove deleted item from list
   */
  function onControlPointDelete() {
    controls.value = controls.value.filter(
      (item) => item.id !== toBeDeleted.value
    )
  }

  watch(
    () => taskId.value,
    async () => {
      controlPoint.value.task = taskId.value
      await loadControlPoints()
    }
  )

  return {
    controls,
    controlPoint,
    loadControlPoints,
    createMode,
    addControlPoint,
    cancelCreate,
    toBeDeleted,
    displayConfirmDelete,
    deleteControlPoint,
    onControlPointDelete,
    errors,
  }
}
