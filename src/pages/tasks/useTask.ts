import { computed, ComputedRef, nextTick, Ref, ref, watchEffect } from 'vue'
import { PartialResource, Task, taskClient } from '@/api'
import { ReactiveRecorder } from '@/components/form/ReactiveRecorder'
import { useFormValidation, UseViolation } from '@/components/form'
import { cloneDeep } from 'lodash'
import { AxiosError } from 'axios'
import { updateShortName } from '@/states/ShortNames'
import { taskTree } from '@/states/weekPlanning'

export type UseTask = {
  task: Ref<PartialResource<Task>>
  taskRecorder: ReactiveRecorder<PartialResource<Task>>
  taskLoading: Ref<boolean>
  patchTask: () => Promise<boolean>
  rollbackTask: () => void
  taskErrors: UseViolation['errors']
  spentTime: ComputedRef<null | number>
}

export function useTask(
  taskId: Ref<Task['id']>,
  onTaskNotFound: () => unknown
): UseTask {
  /**
   * Main task resource
   */
  const task = ref<PartialResource<Task>>({
    id: taskId.value,
  })

  /**
   * Track changes on main task
   */
  const taskRecorder = new ReactiveRecorder<PartialResource<Task>>(task)

  const { errors, clearErrors, parseViolations } = useFormValidation()

  const taskLoading = ref<boolean>(true)

  /**
   * Spent time on a task if startDate & endDate are defined: time = endDate - startDate
   */
  const spentTime = computed((): null | number => {
    let time: null | number = null

    if (task.value && task.value.startDate && task.value.endDate) {
      time =
        (task.value.endDate.getTime() - task.value.startDate.getTime()) / 1000
    }

    return time
  })

  /**
   * Get fresh task from API
   */
  async function loadTask() {
    if (!taskId.value) return

    try {
      taskLoading.value = true
      task.value = await taskClient.getItem(taskId.value)
      taskRecorder.reset()
    } catch (e) {
      if ((e as AxiosError)?.response?.status === 404) {
        onTaskNotFound()
      } else {
        console.error(e)
      }
    } finally {
      taskLoading.value = false
    }
  }

  /**
   * Refresh on ID change
   */
  watchEffect(loadTask)

  async function patchTask() {
    clearErrors()
    let success = false
    try {
      taskLoading.value = true
      await nextTick()
      if (taskRecorder.result.value.hasChanged) {
        task.value = await taskClient.patch(
          taskId.value,
          taskRecorder.result.value.changes
        )
        updateShortName(task.value)
        taskTree.add(task.value as Task)
        taskRecorder.reset()
      }
      success = true
    } catch (e) {
      if ((e as AxiosError)?.response?.status === 422) {
        parseViolations((e as AxiosError).response?.data.violations)
      } else {
        console.error(e)
      }
    }

    taskLoading.value = false
    return success
  }

  function rollbackTask() {
    clearErrors()
    task.value = cloneDeep(taskRecorder.reference)
  }

  return {
    task: task,
    taskRecorder,
    taskLoading,
    patchTask,
    rollbackTask,
    taskErrors: errors,
    spentTime,
  }
}
