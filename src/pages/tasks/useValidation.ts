import {
  ControlPointValidation,
  controlPointValidationClient,
} from '@/api/resources/controlPointValidation'
import { computed, ComputedRef, ref, Ref, watch } from 'vue'
import { ControlPoint } from '@/api/resources/controlPoint'
import { settings } from '@/states'
import { getShortName } from '@/states/ShortNames'
import moment from 'moment-timezone'
import { VueI18n } from 'vue-i18n'
import { userClient } from '@/api'

export type UseValidation = {
  validations: Ref<ControlPointValidation[]>
  loadValidations: () => void
  buttonLabel: ComputedRef
  handleClick: () => void
  getValidationLabel: (validation: ControlPointValidation) => string
}

export function useValidation(
  controlPoint: Ref<ControlPoint['id']>,
  t: VueI18n['t']
): UseValidation {
  const validations = ref<ControlPointValidation[]>([])
  const hasValidated = ref<ControlPointValidation | undefined>(undefined)

  /**
   * Label of the main action button
   */
  const buttonLabel = computed((): string => {
    let label = t('resource.controlPointValidation.create')

    if (validations.value.length) {
      label = hasValidated.value
        ? t('resource.controlPointValidation.remove')
        : t('resource.controlPointValidation.add')
    }

    return label
  })

  /**
   * Get label for given validation formatted as: <user name> - <validation date>
   */
  function getValidationLabel(validation: ControlPointValidation): string {
    return (
      getShortName(userClient.getQualifiedId(validation.verifier)).value +
      ' - ' +
      moment(validation.createdAt).format('LLL')
    )
  }

  /**
   * Add or remove validation, depending on whether user already validated control point
   */
  async function handleClick() {
    if (!hasValidated.value) {
      const resource = await controlPointValidationClient.create({
        controlPoint: controlPoint.value,
      })

      validations.value.push(resource)
      hasValidated.value = resource
    } else {
      await controlPointValidationClient.delete(hasValidated.value.id)

      validations.value = validations.value.filter(
        (item) => item !== hasValidated.value
      )
      hasValidated.value = undefined
    }
  }

  async function loadValidations() {
    const { resources } = await controlPointValidationClient.getCollection({
      controlPoint: controlPoint.value,
    })

    validations.value = resources
  }

  watch(
    () => controlPoint.value,
    async () => await loadValidations()
  )

  watch(
    () => validations.value,
    () => {
      hasValidated.value = validations.value.find((item) => {
        return userClient.getQualifiedId(item.verifier) === settings.user.id
      })
    }
  )

  return {
    validations,
    loadValidations,
    buttonLabel,
    handleClick,
    getValidationLabel,
  }
}
