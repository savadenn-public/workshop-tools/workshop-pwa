<template>
  <aside
    :class="{ Backlog: true, 'Backlog--mini': mini }"
    @dragenter="dragEnter"
    @dragleave="dragLeave"
    @dragover="dragOver"
    @drop="drop"
  >
    <div class="Backlog__Head">
      <PButton
        :title="t('action.clear')"
        class="p-button-text"
        :icon="`mdi mdi-chevron-double-${mini ? 'left' : 'right'}`"
        @click="mini = !mini"
      />

      <div ref="laneElement" class="Backlog__Filter p-inputgroup">
        <PAutoComplete
          v-model="searchTerm"
          v-focus
          :placeholder="searchLabel"
          :suggestions="suggestions"
          :title="searchLabel"
          @complete="suggestCode"
          @item-select="selectCode"
        />
        <PButton
          v-if="searchTerm"
          :title="t('action.clear')"
          class="p-button-warning"
          icon="mdi mdi-close"
          @click="searchTerm = ''"
        />
      </div>
    </div>

    <ul class="Backlog__Tasks">
      <TaskPreview
        v-if="dropTask"
        :task="dropTask"
        class="BusinessCaseLane__Task"
      />
      <TaskComp
        v-for="task in taskTree.backlog.value.values()"
        :key="task.id"
        :task="task"
        class="Backlog__Task"
      />
      <PButton v-if="next" class="p-button-text" @click="next"
        >{{ t('resource.task.loadMore') }}
      </PButton>
    </ul>
  </aside>
</template>

<script lang="ts" setup>
import {
  businessCaseClient,
  GetTasksParam,
  ResourceList,
  Task,
  taskClient,
} from '@/api'
import { onMounted, onUnmounted, reactive, ref, watch } from 'vue'
import { useI18n } from 'vue-i18n'
import TaskComp from './Task.vue'
import TaskPreview from './TaskPreview.vue'
import { taskTree } from '@/states/weekPlanning'
import { taskSearchParse } from '@/pages/weekPlanning/taskSearchParse'
import {
  draggedObject,
  handleTaskMoveAndResize,
} from '@/pages/weekPlanning/handleTaskMoveAndResize'
import { ProvidedResources, ResourcesKey } from '@/provided'
import { injectStrict } from '@/injectStrict'

const { t } = useI18n()

/**
 * if true, api call in progress
 */
const loading = ref(false)

/**
 * Search filter input value typed by user
 */
const searchTerm = ref<string>('')

/**
 * Label for the filter input
 */
const searchLabel = t('resource.task.search')

/**
 * Autocomplete suggestions for business code
 */
const suggestions = ref<string[]>([])

/**
 * Selected businessCase code
 */
const businessCaseCode = ref<string | undefined>('')

const mini = ref(true)

const Resources = injectStrict<ProvidedResources>(ResourcesKey)

/**
 * Suggests business case codes from partial #code
 */
async function suggestCode() {
  const { code, title } = taskSearchParse(searchTerm.value)
  if (code && code !== businessCaseCode.value) {
    suggestions.value = [title || '']
    const { resources } = await businessCaseClient.getCollection({ code })
    resources.forEach((businessCase) => {
      if (businessCase.code) {
        Resources.bufferizeBusinessCase(businessCase)
        suggestions.value.push(`#${businessCase.code} ${title || ''}`)
      }
    })
  }
}

/**
 * Select one of the suggested code
 */
async function selectCode(param: { value: string }) {
  const { code } = taskSearchParse(param.value)
  businessCaseCode.value = code
}

/**
 * Parameters to search for tasks
 */

const queryParams = reactive<GetTasksParam>({
  page: 1,
  title: '',
  'exists[startDate]': false,
  archived: false,
})

/**
 * Function to call to lodd next page of results
 */
const next = ref<(() => Promise<void>) | undefined>(undefined)

/**
 * Parse results of task getCollection
 * @param result
 */
function parseResult(result: ResourceList<Task>) {
  result.resources.forEach((task) => taskTree.add(task))
  next.value = undefined
  if (result.next) {
    next.value = async () => {
      if (result.next) {
        parseResult(await result.next())
      }
    }
  }
}

/**
 * Ref to element enclosing tasks
 */
const laneElement = ref<HTMLElement>()

/**
 * Composition API : handle drag&drop a task
 */
const { active, dragEnter, dragLeave, dragOver, dropTask } =
  handleTaskMoveAndResize(laneElement)

/**
 * On drop, unassigned Task and unscheduled
 */
const drop = () => {
  active.value = false
  // get the draggable element
  if (draggedObject.task) {
    dropTask.value = undefined
    taskTree.patch(draggedObject.task.id, { startDate: null })
  }
}

/**
 * Load all tasks of the business case
 */
async function loadData() {
  loading.value = true
  try {
    let params: GetTasksParam = {
      ...queryParams,
      title: searchTerm.value,
    }
    const { code, title } = taskSearchParse(searchTerm.value)
    if (code) {
      params = {
        ...queryParams,
        businessCase: await Resources.getBusinessCaseId(code),
        title,
      }

      if (!params.businessCase) {
        // Code is not found, not necessary to call for tasks
        taskTree.backlog.value.clear()
        next.value = undefined
        loading.value = false
        return
      }
    }

    let result = await taskClient.getCollection(params)
    taskTree.backlog.value.clear()
    parseResult(result)
  } finally {
    loading.value = false
  }
}

/**
 * First search at load time
 */
onMounted(loadData)

/**
 * Debounced search
 */
let timeout = 0
watch(searchTerm, () => {
  if (timeout) window.clearTimeout(timeout)
  timeout = window.setTimeout(loadData, 300)
})
onUnmounted(() => window.clearTimeout(timeout))
</script>

<style lang="scss" scoped>
@use 'src/style/variables' as v;

.Backlog {
  $root: &;

  display: grid;
  grid-area: r-sideBar;
  grid-template-rows: minmax(min-content, auto) 1fr;
  overflow-y: auto;
  max-width: 30vw;
  border-left: solid 1px var(--surface-d);
  background: var(--surface-100);

  &__Head {
    display: inline-flex;
    padding: v.$spacing-2;
    border-radius: 0;
  }

  &--mini {
    #{$root}__Head {
      padding: v.$spacing-2;
    }

    #{$root}__Filter {
      display: none;
    }

    #{$root}__Tasks {
      max-width: 50px;
    }
  }

  &__Tasks {
    display: flex;
    flex-direction: column;
    grid-gap: v.$spacing-4;
    overflow-x: hidden;
    margin: 0;
    padding: v.$spacing-2;
    border-radius: 0;
    list-style: none;
  }

  &__Task {
    border: none;
    background: var(--task-background-color, var(--surface-0));
  }
}
</style>
