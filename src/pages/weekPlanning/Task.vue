<template>
  <component
    :is="props.tag"
    ref="taskElement"
    :class="getClass"
    :draggable="!isClosed"
    :title="props.task.title"
    :style="style"
    @dragend="end"
    @dragstart="move"
    @mouseover="handleMouseOver"
    @mouseleave="handleMouseLeave"
  >
    <!-- .prevent: prevent opening link while drag&dropping -->
    <router-link
      class="Task__Title"
      :to="{ name: ROUTES.TASK.name, params: { uuid: props.task.id } }"
      @mousedown.prevent="() => {}"
      @mouseup.prevent="() => {}"
    >
      <i
        v-if="overdue && !isClosed"
        class="mdi mdi-fire-alert Task__Overdue"
        :title="t('resource.task.overdue')"
      />
      {{ label }}
    </router-link>
    <span class="Task__Subtitle">{{ subtitle }}</span>
    <span
      v-if="!isClosed && props.resizable"
      class="Task__Resize"
      @mousedown.prevent="resize"
    />
    <i
      v-if="isClosed"
      class="mdi mdi-check-circle-outline Task__Closed"
      :title="t('resource.task.closed')"
    />
  </component>
</template>

<script lang="ts" setup>
import { BY_WORK_FORCE, Task } from '@/api'
import { computed, ref, toRef, toRefs } from 'vue'
import { useI18n } from 'vue-i18n'
import { ROUTES } from '@/plugins'
import {
  handleOutOfBound,
  positionToMoment,
} from '@/pages/weekPlanning/timedPosition'
import { draggedObject } from '@/pages/weekPlanning/handleTaskMoveAndResize'
import { useTaskLabel } from '@/pages/weekPlanning/useTaskLabel'
import { useTaskColor } from '@/pages/weekPlanning/useTaskColor'
import { injectStrict } from '@/injectStrict'
import { FocusIdKey, ProvidedResources, ResourcesKey } from '@/provided'
import { config } from '@/states/weekPlanning'

const focusId = injectStrict(FocusIdKey)
const props = withDefaults(
  defineProps<{
    task: Task
    tag?: string
    resizable?: boolean
  }>(),
  {
    tag: 'li',
    resizable: false,
  }
)
const { task } = toRefs(props)

const emits = defineEmits<{
  /**
   * Fire when the user is resizing the task
   * It contains the newEstimatedEndDate
   */
  (e: 'move:estimatedEndDate', params: { task: Task; date: Date }): void

  /**
   * Fire when the user has finished resizing the task (mouse release)
   * It contains either the newEstimatedEndDate or null if resizing is cancelled
   */
  (e: 'end:estimatedEndDate', params: { task: Task; date?: Date }): void
}>()

const { t } = useI18n()
const moving = ref(false)
const taskElement = ref<HTMLElement>()

const { startBefore, endAfter } = handleOutOfBound(toRef(props, 'task'))

const getClass = computed(() => ({
  Task: true,
  'Task--moving': moving.value,
  'Task--startBefore': startBefore.value,
  'Task--endAfter': endAfter.value,
  'Task--resizable': props.resizable,
  'Task--closed': isClosed.value,
  'Task--hidden': isHidden.value,
}))

/**
 * Whether the task should be hidden
 */
const isHidden = computed((): boolean => {
  return (
    task.value.startDate instanceof Date &&
    undefined !== focusId.value &&
    focusId.value !== getFocusableId()
  )
})

function move(e: DragEvent) {
  if (isClosed.value) return

  draggedObject.task = { ...props.task }
  draggedObject.offset = 0

  if (e.dataTransfer) {
    e.dataTransfer.dropEffect = 'move'
    e.dataTransfer.effectAllowed = 'move'
  }

  if (taskElement.value) {
    draggedObject.offset = e.x - taskElement.value.getBoundingClientRect().x
  }

  /* Chrome fire a end directly
   * Workaround add timeout
   * https://stackoverflow.com/questions/19639969/html5-dragend-event-firing-immediately?answertab=votes#tab-top
   */
  window.setTimeout(() => {
    moving.value = true
  }, 0)
}

/**
 * Whether the task is closed
 */
const isClosed = computed(() => Boolean(task.value.endDate))
const Resources = injectStrict<ProvidedResources>(ResourcesKey)
const businessCase = Resources.getCachedBusinessCase(task.value.businessCase)

/**
 * Whether task ends after business case due date
 */
const overdue = computed((): boolean => {
  let overdue = false

  if (businessCase.value?.dueDate && task.value.estimatedEndMin) {
    overdue =
      businessCase.value.dueDate.getTime() <
      task.value.estimatedEndMin.getTime()
  }

  return overdue
})

/**
 * Get Date from mouse position
 */
function getDateOnMousePosition(e: MouseEvent) {
  const parent = taskElement.value?.parentElement
  return parent ? positionToMoment(e, parent, false) : null
}

/**
 * Handle moving mouse while resizing
 */
function onMove(e: MouseEvent) {
  if (overTimer) clearTimeout(overTimer)
  const endMoment = getDateOnMousePosition(e)
  if (endMoment && endMoment.isAfter(props.task.startDate, 'minute')) {
    emits('move:estimatedEndDate', {
      task: props.task,
      date: endMoment.toDate(),
    })
  }
}

const resize = () => {
  moving.value = true
  if (!props.task.estimatedEndMax) return
  document.addEventListener('mousemove', onMove)
  document.addEventListener(
    'mouseup',
    (e: MouseEvent) => {
      moving.value = false
      const endMoment = getDateOnMousePosition(e)
      document.removeEventListener('mousemove', onMove)
      emits('end:estimatedEndDate', {
        task: props.task,
        date: endMoment?.toDate(),
      })
    },
    { once: true }
  )
}

let overTimer = 0

/**
 * Show tasks of the same BusinessCase|Workforce when overing for 1 second
 */
function handleMouseOver() {
  // Skip this behavior for tasks in backlog
  if (!task.value.startDate) return

  if (overTimer) window.clearTimeout(overTimer)

  overTimer = window.setTimeout(() => {
    focusId.value = getFocusableId()
  }, 1000)
}

/**
 * Reset visibility of tasks
 */
function handleMouseLeave() {
  if (overTimer) clearTimeout(overTimer)
  focusId.value = undefined
}

/**
 * Return ID of the BusinessCase|Assignee of the task, depending on planning config
 */
function getFocusableId(): string | undefined {
  const id =
    config.row === BY_WORK_FORCE
      ? task.value.businessCase
      : task.value.assignedTo

  return id || undefined
}

const end = () => {
  if (isClosed.value) return
  draggedObject.task = undefined
  draggedObject.offset = 0
  moving.value = false
}

const { label, subtitle } = useTaskLabel(task)

const { style } = useTaskColor(task)
</script>

<style lang="scss" scoped>
@use './task' as t;
@use 'src/style/variables' as v;

.Task {
  $root: &;

  @include t.task;

  &--moving {
    display: none;
  }

  &--resizable {
    padding-right: 0;
    resize: horizontal;
  }

  &--closed {
    border-width: 0;
    background: var(--success-color);
    color: var(--primary-color-text);
    cursor: not-allowed;

    // Force color of triangle
    &::before,
    &::after {
      background: var(--success-color);
    }
  }

  &__Closed {
    grid-area: handle;
    align-self: start;
    margin-right: v.$spacing-2;
    cursor: help;
  }

  &[draggable='true'] {
    cursor: move;
  }

  &__Resize {
    grid-area: handle;
    width: v.$spacing;
    height: 100%;
    font-size: 2rem;
    cursor: e-resize;
  }
}
</style>
