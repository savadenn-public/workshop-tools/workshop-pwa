import { taskSearchParse } from '@/pages/weekPlanning/taskSearchParse'

describe('taskSearchParse', function () {
  test('simple term', () => {
    expect(taskSearchParse('my term')).toEqual({
      title: 'my term',
      code: undefined,
    })

    expect(taskSearchParse('  my term')).toEqual({
      title: 'my term',
      code: undefined,
    })

    expect(taskSearchParse('my term  ')).toEqual({
      title: 'my term',
      code: undefined,
    })
  })

  test('simple code', () => {
    expect(taskSearchParse('#code')).toEqual({
      title: undefined,
      code: 'code',
    })

    expect(taskSearchParse(' #code')).toEqual({
      title: undefined,
      code: 'code',
    })

    expect(taskSearchParse('#code ')).toEqual({
      title: undefined,
      code: 'code',
    })
  })

  test('complete', () => {
    expect(taskSearchParse('#code    term')).toEqual({
      title: 'term',
      code: 'code',
    })

    expect(taskSearchParse('   #code term')).toEqual({
      title: 'term',
      code: 'code',
    })

    expect(taskSearchParse('#code term   ')).toEqual({
      title: 'term',
      code: 'code',
    })
  })
})
