import { Ref, ref } from 'vue'
import { Task } from '@/api'
import { positionToMoment } from '@/pages/weekPlanning/timedPosition'
import { taskTree } from '@/states/weekPlanning'
import { cloneDeep } from 'lodash'
import moment from 'moment-timezone'

/**
 * Can accept drag&drop
 */
type TaskMoveAndResizeHandler = {
  active: Ref<boolean>
  dragEnter: (e: DragEvent) => void
  dragLeave: (e: DragEvent) => void
  dragOver: (e: DragEvent) => void
  dropTask: Ref<Task | undefined>
  handleMoveMax: (params: { task: Task; date: Date }) => void
  handleEndMax: (params: { task: Task; date?: Date }) => void
}

type DraggedObject = { task?: Task; offset: number }
export const draggedObject: DraggedObject = {
  offset: 0,
}

/**
 * Vue component is a target for drag&drop event
 *
 * @param targetRef
 * @param valid Function, returns true if dragged element can be dropped on component
 */
export function handleTaskMoveAndResize(
  targetRef: Ref<HTMLElement | undefined>,
  valid: (draggedTask?: Task) => boolean = () => true
): TaskMoveAndResizeHandler {
  const active = ref(false)

  const dragEnter = (e: DragEvent) => {
    if (!targetRef.value) return
    e.preventDefault()
    if (draggedObject.task && valid(draggedObject.task)) {
      active.value = true
      const startDate = positionToMoment(e, targetRef.value)
      dropTask.value = {
        ...draggedObject.task,
        startDate: startDate.toDate(),
        estimatedEndMax: startDate
          .clone()
          .add(draggedObject.task.durationMax, 'minutes')
          .toDate(),
      }
    }
  }

  const dragLeave = (e: DragEvent) => {
    e.preventDefault()
    active.value = false
    dropTask.value = undefined
  }

  const dragOver = (e: DragEvent) => {
    if (!dropTask.value) return dragEnter(e)
    if (!targetRef.value) return
    e.preventDefault()
    if (draggedObject.task && valid(draggedObject.task)) {
      const startDate = positionToMoment(e, targetRef.value)
      dropTask.value.startDate = startDate.toDate()
      dropTask.value.estimatedEndMax = startDate
        .clone()
        .add(dropTask.value.durationMax, 'minutes')
        .toDate()
    }
  }

  const dropTask = ref<Task | undefined>(undefined)

  function resizePatch(params: { task: Task; date: Date }) {
    const maxDate = moment(params.date)
    const durationMax = maxDate.diff(params.task.startDate, 'minutes', false)
    const patch: Partial<Task> = {
      durationMax,
      durationMin: Math.min(params.task.durationMin || 15, durationMax),
    }
    return patch
  }

  function handleMoveMax(params: { task: Task; date: Date }) {
    active.value = true
    if (!dropTask.value) dropTask.value = cloneDeep(params.task)

    const patch = resizePatch(params)
    dropTask.value.estimatedEndMax = moment(dropTask.value.startDate)
      .clone()
      .add(dropTask.value.durationMax, 'minutes')
      .toDate()
    dropTask.value.durationMin = patch.durationMin
    dropTask.value.durationMax = patch.durationMax
  }

  function handleEndMax(params: { task: Task; date?: Date }) {
    if (dropTask.value) {
      dropTask.value = undefined
      active.value = false
      if (params.date && params.task.startDate) {
        taskTree.patch(
          params.task.id,
          resizePatch({
            date: params.date,
            task: params.task,
          })
        )
      }
    }
  }

  return {
    active,
    dragEnter,
    dragOver,
    dragLeave,
    dropTask,
    handleMoveMax,
    handleEndMax,
  }
}
