import { computed, Ref } from 'vue'
import chroma from 'chroma-js'
import { DEFAULT_COLORS } from '@/pages/DEFAULT_COLORS'
import { Task } from '@/api'
import { injectStrict } from '@/injectStrict'
import { ProvidedResources, ResourcesKey } from '@/provided'

const BLACK = '#000000'
const WHITE = '#ffffff'

export function useTaskColor(task: Ref<Task>) {
  const Resources = injectStrict<ProvidedResources>(ResourcesKey)
  const businessCase = Resources.getCachedBusinessCase(task.value.businessCase)

  const backgroundColor = computed(() => businessCase.value.color || null)

  const secondaryColor = computed(() => {
    if (!backgroundColor.value) return null

    for (const defaultColors of DEFAULT_COLORS) {
      if (defaultColors.primary === backgroundColor.value) {
        return defaultColors.secondary
      }
    }

    return chroma(backgroundColor.value).brighten(2).hex()
  })

  const textColor = computed(() => {
    if (!businessCase.value.color) return null
    return chroma.contrast(businessCase.value.color, BLACK) >
      chroma.contrast(businessCase.value.color, WHITE)
      ? BLACK
      : WHITE
  })

  const style = computed(() => {
    const style = []
    if (backgroundColor.value)
      style.push(`--task-background-color: ${backgroundColor.value};`)
    if (secondaryColor.value)
      style.push(`--task-secondary-color: ${secondaryColor.value};`)
    if (secondaryColor.value)
      style.push(`--task-text-color: ${textColor.value}`)

    return style.join('')
  })

  return {
    backgroundColor,
    secondaryColor,
    textColor,
    style,
  }
}
