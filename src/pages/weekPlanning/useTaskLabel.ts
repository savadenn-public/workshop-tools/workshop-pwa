import { computed, Ref, ref, watchEffect } from 'vue'
import type { Task } from '@/api'
import { BY_WORK_FORCE } from '@/api'
import {
  ProvidedResources,
  ProvidedWeekPlanning,
  ResourcesKey,
  WeekPlanningKey,
} from '@/provided'
import { injectStrict } from '@/injectStrict'

/**
 * Label: title of the task
 *
 * Subtitle: either assignee name or businessCase code, depending
 * on the view Planning view (by Workforce or by Business Case)
 *
 */
export type UseTaskLabel = {
  label: Ref<string>
  subtitle: Ref<string>
}

export function useTaskLabel(task: Ref<Task>): UseTaskLabel {
  const label = ref('')
  const assignee = ref('')

  const Resources = injectStrict<ProvidedResources>(ResourcesKey)
  const WeekPlanning = injectStrict<ProvidedWeekPlanning>(WeekPlanningKey)

  const businessCase = Resources.getCachedBusinessCase(task.value.businessCase)

  watchEffect(async () => {
    assignee.value = task.value.assignedTo
      ? await Resources.getWorkforceName(task.value.assignedTo)
      : '-'
    label.value = task.value.title
  })

  return {
    label,
    subtitle: computed(() =>
      WeekPlanning.config.row === BY_WORK_FORCE
        ? `${businessCase.value.name ?? '-'} #${businessCase.value.code ?? '-'}`
        : assignee.value
    ),
  }
}
