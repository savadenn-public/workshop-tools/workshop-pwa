/* istanbul ignore file */
import * as Page from '@/pages'
import {
  createRouter,
  createWebHashHistory,
  RouteLocationNormalized,
  RouteLocationRaw,
  RouteRecordRaw,
  RouterOptions,
} from 'vue-router'
import { getType, ResourceType, userClient } from '@/api'
import { addMessage, updateCurrentUser } from '@/states'
import { i18n } from '@/plugins/i18n'
import { NamedRoute, ROUTES } from '@/plugins/routes'

const routes: RouteRecordRaw[] = [
  <RouteRecordRaw>{
    ...ROUTES.HOME,
    path: '/home',
    component: Page.Home,
  },
  <RouteRecordRaw>{
    ...ROUTES.SETTINGS,
    path: '/settings',
    component: Page.Settings,
  },
  <RouteRecordRaw>{
    ...ROUTES.BUSINESS_CASE,
    path: '/business-cases/:uuid',
    component: Page.BusinessCase,
    props: true,
  },
  <RouteRecordRaw>{
    ...ROUTES.BUSINESS_CASES,
    path: '/business-cases',
    component: Page.BusinessCases,
  },
  <RouteRecordRaw>{
    ...ROUTES.TASK,
    path: '/tasks/:uuid',
    component: Page.Task,
    props: true,
  },
  <RouteRecordRaw>{
    ...ROUTES.WEEK_PLANNING,
    path: '/week-planning/:idSettings?',
    component: Page.WeekPlanning,
    props: true,
  },
  <RouteRecordRaw>{
    ...ROUTES.MACHINES,
    path: '/admin/machines',
    component: Page.Machines,
  },
  <RouteRecordRaw>{
    ...ROUTES.TEAMS,
    path: '/admin/teams',
    component: Page.Teams,
  },
  <RouteRecordRaw>{
    ...ROUTES.USERS,
    path: '/admin/users',
    component: Page.Users,
    meta: {
      admin: true,
    },
  },
  <RouteRecordRaw>{
    ...ROUTES.APIKEYS,
    path: '/admin/apiKeys',
    component: Page.ApiKeys,
    meta: {
      admin: true,
    },
  },
  <RouteRecordRaw>{
    ...ROUTES.ADMINISTRATION,
    path: '/admin',
    redirect: ROUTES.USERS,
    meta: {
      admin: true,
    },
  },
  <RouteRecordRaw>{
    ...ROUTES.RESOURCE,
    path: '/resource/:uuid',
    redirect: redirectToResource,
  },
  <RouteRecordRaw>{
    path: '/:pathMatch(.*)*',
    redirect: '/home',
  },
]

export const Router = createRouter(<RouterOptions>{
  history: createWebHashHistory(),
  routes,
})

// https://next.router.vuejs.org/guide/advanced/navigation-guards.html#navigation-guards
Router.beforeEach((to) => {
  if (handleConfirmEmail(to)) return '/'
})

/**
 * Consume token for an email confirmation change if url matches, return false otherwise
 */
function handleConfirmEmail(to: RouteLocationNormalized): boolean {
  const token = to.query['confirm_email_token']
  if (!token) return false

  userClient
    .consumeConfirmEmailToken(token as string)
    .then(() => {
      addMessage({
        severity: 'success',
        summary: i18n.global.t('toast.success.change_email.confirmed.summary'),
        detail: i18n.global.t('toast.success.change_email.confirmed.detail'),
        life: 6000,
      })
    })
    .then(() => updateCurrentUser())

  return true
}

/**
 * Resource route mapping
 */
const RESOURCE_ROUTING: Record<ResourceType, NamedRoute | undefined> = {
  [ResourceType.User]: undefined,
  [ResourceType.Task]: ROUTES.TASK,
  [ResourceType.BusinessCase]: ROUTES.BUSINESS_CASE,
  [ResourceType.team]: ROUTES.TEAMS,
  [ResourceType.machine]: ROUTES.MACHINES,
  [ResourceType.workforce]: undefined,
  [ResourceType.apiKey]: ROUTES.APIKEYS,
}

/**
 * Redirect to given resource page
 */
function redirectToResource(to: RouteLocationNormalized): RouteLocationRaw {
  const id = to.params.uuid as string
  const type = getType({ id })
  if (!type || !RESOURCE_ROUTING[type]) return ROUTES.HOME
  return { ...RESOURCE_ROUTING[type], params: { uuid: id } }
}
