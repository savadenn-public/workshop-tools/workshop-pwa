export type NamedRoute = { name: string }

export const ROUTES: Record<string, NamedRoute> = {
  ADMINISTRATION: { name: 'redirect-to-first-admin-page' },
  APIKEYS: { name: 'page.apiKeys.title' },
  HOME: { name: 'page.home.title' },
  SETTINGS: { name: 'page.settings.title' },
  BUSINESS_CASE: { name: 'page.business_case.title' },
  BUSINESS_CASES: { name: 'page.business_cases.title' },
  MACHINES: { name: 'page.machines.title' },
  RESOURCE: { name: 'page.resource.title' },
  USERS: { name: 'page.users.title' },
  TASK: { name: 'page.task.title' },
  TEAMS: { name: 'page.teams.title' },
  WEEK_PLANNING: { name: 'page.weekPlanning.title' },
  WORKFORCES: { name: 'page.workforces.title' },
}
