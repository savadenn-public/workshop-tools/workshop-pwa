import { Now, Resources, RoleManager, settings, WeekPlanning } from '@/states'
import { InjectionKey, Ref } from 'vue'

export const ResourcesKey: InjectionKey<typeof Resources> = Symbol('Resources')
export const NowKey: InjectionKey<typeof Now> = Symbol('Now')
export const SettingsKey: InjectionKey<typeof settings> = Symbol('Settings')
export const RoleManagerKey: InjectionKey<RoleManager> = Symbol('RoleManager')
export const WeekPlanningKey: InjectionKey<typeof WeekPlanning> =
  Symbol('WeekPlanning')
export const FocusIdKey: InjectionKey<Ref<string | undefined>> =
  Symbol('FocusId')

export type ProvidedResources = typeof Resources
export type ProvidedNow = typeof Now
export type ProvidedSettings = typeof settings
export type ProvidedWeekPlanning = typeof WeekPlanning
