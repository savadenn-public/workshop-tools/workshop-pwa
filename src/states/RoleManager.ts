import type { Role } from '@/api'
import { computed, ref } from 'vue'
import { ROLE_ADMIN, loadRolesFromToken } from '@/api'

export class RoleManager {
  readonly roles = ref<Role[]>([])

  isAdmin = computed(() => this.roles.value.includes(ROLE_ADMIN))

  async loadRoles() {
    await loadRolesFromToken()
  }
}

export const roleManager = new RoleManager()
