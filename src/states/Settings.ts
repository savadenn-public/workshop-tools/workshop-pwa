import { reactive, watch, watchEffect } from 'vue'
import { Browser } from '.'
import { PlanningSettings, User, userClient } from '@/api'
import * as Types from '@/types'
import { Resources, roleManager } from '@/states'

export interface Settings {
  lang: string
  timeZone: string
  theme: Types.Theme
  user: User
  miniMenu: boolean
  refreshToken: string | null
  planningSettings: PlanningSettings | null
}

const defaultUser = (): User => ({
  id: '',
  name: '',
  email: '',
  roles: [],
  avatar: null,
  lockDate: null,
})

export const DEFAULT_SETTINGS: Settings = {
  lang: Browser.getLang(),
  theme: Browser.getTheme(),
  timeZone: Browser.getTimeZone(),
  user: defaultUser(),
  miniMenu: false,
  refreshToken: null,
  planningSettings: null,
}

export function resetSettings(): void {
  Object.assign(settings, DEFAULT_SETTINGS)
  settings.refreshToken = null
}

export const settings: Settings = reactive(DEFAULT_SETTINGS)

// Load saved settings
const SETTINGS_KEY = 'settings'

/**
 * Gets latest data for current user
 */
export async function updateCurrentUser(): Promise<void> {
  if (settings.user.id)
    settings.user =
      (await userClient.getItem(settings.user.id)) ?? defaultUser()
}

export async function loadSavedSettings(): Promise<void> {
  await roleManager.loadRoles()

  // Run on user change, like log in
  watch(
    () => settings.user?.id,
    async (id, oldId) => {
      if (id && id !== oldId) {
        await updateCurrentUser()
        await roleManager.loadRoles()
        await Resources.initWorkforces()
      }
    }
  )

  const savedSettingsJSON = localStorage.getItem(SETTINGS_KEY)
  const savedSettings = savedSettingsJSON ? JSON.parse(savedSettingsJSON) : {}
  if (savedSettings) {
    Object.keys(settings).forEach((key) => {
      if (savedSettings[key])
        Object.assign(settings, { [key]: savedSettings[key] })
    })
  }

  // On change save settings
  watchEffect(() =>
    localStorage.setItem(SETTINGS_KEY, JSON.stringify(settings))
  )
}
