import {
  BusinessCase,
  getApiClient,
  getType,
  Machine,
  Resource,
  ResourceType,
  Task,
  Team,
  User,
  Workforce,
} from '@/api'
import { Ref, ref } from 'vue'

const shortNameCache = new Map<Resource['id'], Ref<string>>()
const DEFAULT_NAME = '-'
/**
 * Query resource and add shortname to cache
 */
async function getFreshShortName(id: Resource['id']): Promise<void> {
  const client = await getApiClient()
  const { data } = await client.get(id)
  updateShortName({ ...data, id: data['@id'] })
}

/**
 * Parse any resource and return its short name
 */
function parseShortName(resource: Resource) {
  const type = getType(resource)

  switch (type) {
    case ResourceType.BusinessCase:
      return `${(resource as BusinessCase).name} #${
        (resource as BusinessCase).code
      }`
    case ResourceType.Task:
      return (resource as Task).title
    case ResourceType.workforce:
      return (resource as Workforce).name
    case ResourceType.team:
      return (resource as Team).name
    case ResourceType.machine:
      return (resource as Machine).name
    case ResourceType.User:
      return (resource as User).name || (resource as User).email
    default:
      return DEFAULT_NAME
  }
}

/**
 * Returns short name for given resource id
 */
export function getShortName(id: Resource['id']): Ref<string> {
  let shortName = shortNameCache.get(id)
  if (!shortName) {
    shortName = ref(DEFAULT_NAME)
    shortNameCache.set(id, shortName)

    // Get short name asynchronously
    getFreshShortName(id).catch((e) => console.error(e))
  }
  return shortName
}

/**
 * Updates short name of given resource
 */
export function updateShortName(resource: Resource): void {
  let shortName = shortNameCache.get(resource.id)
  if (!shortName) {
    shortName = ref(DEFAULT_NAME)
    shortNameCache.set(resource.id, shortName)
  }
  shortName.value = parseShortName(resource)
}
