import { addMessage, Message, messages } from '@/states'

describe('Messages', () => {
  test('addMessage', () => {
    expect(messages.value.length).toBe(0)

    const message: Message = {
      severity: 'success',
      summary: 'Hello moto',
      detail: 'Some details',
      life: 3000,
    }

    addMessage(message)

    expect(messages.value.length).toBe(1)
    const actual = messages.value.pop()
    expect(actual).toStrictEqual(message)
  })
})
