import { BusinessCase, businessCaseClient } from '@/api'
import { ref, Ref } from 'vue'

const DEFAULT_NAME = '-'

const businessCases = new Map<BusinessCase['id'], Ref<BusinessCase>>()
const businessCaseCodeIds = new Map<BusinessCase['code'], BusinessCase['id']>()

/**
 * Get BusinessCase code with local cache
 */
export function getCachedBusinessCase(id: string): Ref<BusinessCase> {
  let businessCase = businessCases.get(id)
  if (!businessCase) {
    businessCase = ref({
      code: DEFAULT_NAME,
      id: '',
      color: null,
      name: DEFAULT_NAME,
      createdAt: new Date(),
      tasks: [],
      dueDate: null,
      archiveDate: null,
    })
    businessCases.set(id, businessCase)

    businessCaseClient.getItem(id).then(bufferizeBusinessCase)
  }
  return businessCase
}

/**
 * Get BusinessCase code with local cache
 */
export async function getBusinessCaseId(
  code: string
): Promise<BusinessCase['id'] | undefined> {
  const businessCaseId = businessCaseCodeIds.get(code)
  if (businessCaseId) return businessCaseId

  const { resources } = await businessCaseClient.getCollection({ code })
  resources.forEach(bufferizeBusinessCase)

  return businessCaseCodeIds.get(code)
}

/**
 * Add given business case to buffer
 */
export function bufferizeBusinessCase(businessCase: BusinessCase): void {
  const cachedBusinessCase = businessCases.get(businessCase.id)
  if (cachedBusinessCase) {
    cachedBusinessCase.value = businessCase
  } else {
    businessCases.set(businessCase.id, ref(businessCase))
  }

  businessCaseCodeIds.set(businessCase.code, businessCase.id)
}
