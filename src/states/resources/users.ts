import type { User, Avatar, Resource } from '@/api/resources'
import { ref, Ref } from 'vue'
import { userClient, avatarClient } from '@/api'

export type UserWithAvatar = {
  user: Partial<User> & Resource
  avatar: Avatar | null
}

const userWithAvatars = new Map<User['id'], Ref<UserWithAvatar>>()

/**
 * Load API data onto given reactive object
 */
async function loadUserWithAvatar(userWithAvatar: Ref<UserWithAvatar>) {
  const user = await userClient.getItem(userWithAvatar.value.user.id)
  if (user) {
    const newUser: UserWithAvatar = { user, avatar: null }
    if (newUser.user.avatar) {
      newUser.avatar = await avatarClient.getItem(newUser.user.avatar)
    }
    userWithAvatar.value = newUser
  }
}

/**
 * Return reactive user and avatar for given ID
 */
export function getUserWithAvatar(
  userId: User['id'],
  forceUpdate = false
): Ref<UserWithAvatar> {
  let userWithAvatar = userWithAvatars.get(userId)
  let found = true
  if (!userWithAvatar) {
    found = false
    userWithAvatar = ref<UserWithAvatar>({
      user: { id: userId },
      avatar: null,
    })
    userWithAvatars.set(userId, userWithAvatar)
  }

  if (!found || forceUpdate) {
    loadUserWithAvatar(userWithAvatar).catch(console.error)
  }

  return userWithAvatar
}
