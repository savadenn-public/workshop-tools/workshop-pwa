import { computed, ref, watchEffect } from 'vue'
import {
  BusinessCase,
  BY_WORK_FORCE,
  ResourceMap,
  Task,
  taskClient,
  Workforce,
} from '@/api/resources'
import { config, maxDate, minDate } from '.'
import { WatchStopHandle } from '@vue/runtime-core'
import { Resources } from '@/states'
import moment from 'moment-timezone'
import { StopHandler } from '@/api/resources/watchableResource'

type ByBusinessCase = Map<BusinessCase['id'], ResourceMap<Task>>

// Allow id null for unassigned tasks
type ByWorkforce = Map<Workforce['id'] | null, ResourceMap<Task>>

export class TaskTree {
  /**
   * Tasks by their BusinessCase
   */
  byBusinessCase = ref<ByBusinessCase>(
    new Map<BusinessCase['id'], ResourceMap<Task>>()
  )

  /**
   * Handle to stop watching workForces
   * @private
   */
  private stopHandleWatch?: WatchStopHandle

  constructor() {
    this.clear()
  }

  /**
   * Tasks by their assignee
   */
  byWorkforce = ref<ByWorkforce>(
    new Map<Workforce['id'] | null, ResourceMap<Task>>()
  )

  /**
   * Direct task pointers
   * @private
   */
  private tasks: ResourceMap<Task> = new Map<Task['id'], Task>()

  /**
   * Stop watching changes on tasks
   */
  private stopHandleLiveReload: StopHandler | null = null

  /**
   * Tasks without any startDate
   * @private
   */
  backlog = ref(new Map<Task['id'], Task>())

  /**
   * Current groups relative to mode
   */
  current = computed<Map<Workforce['id'] | null, ResourceMap<Task>>>(
    () => this.getMode().value
  )

  protected getMode() {
    if (config.row === BY_WORK_FORCE) return this.byWorkforce
    return this.byBusinessCase
  }

  /**
   * Returns the number of tasks
   */
  get size(): number {
    let total = 0
    this.byWorkforce.value.forEach((taskMap) => {
      total += taskMap.size
    })
    this.byBusinessCase.value.forEach((taskMap) => {
      total += taskMap.size
    })
    return total
  }

  /**
   * Starts live reloading changes on tasks
   */
  async startLiveReload(): Promise<void> {
    if (this.stopHandleLiveReload) return

    this.stopHandleLiveReload = await taskClient.watchAll(async (id) => {
      const task = await taskClient.getItem(id)
      if (task) this.add(task)
    })
  }

  /**
   * Stops live reload
   */
  stopLiveReload() {
    if (!this.stopHandleLiveReload) return
    this.stopHandleLiveReload()
    this.stopHandleLiveReload = null
  }

  /**
   * Clear the tree, empty all task & BusinessCases
   * keeps workforces
   */
  clear(clearBacklog = false): void {
    // First let's stop the watchEffect
    if (this.stopHandleWatch) this.stopHandleWatch()

    if (clearBacklog) {
      this.backlog.value.clear()
    }
    this.byWorkforce.value.clear()
    this.byBusinessCase.value.clear()
    this.tasks.clear()

    // initiates the `NotAssigned` group
    this.byWorkforce.value.set(null, new Map<Task['id'], Task>())

    this.stopHandleWatch = watchEffect(() => {
      for (const workforceId of Resources.allId.value) {
        if (
          !this.byWorkforce.value.has(workforceId) &&
          (!config.selectedWorkforces.length ||
            config.selectedWorkforces.includes(workforceId))
        ) {
          this.byWorkforce.value.set(workforceId, new Map<Task['id'], Task>())
        }
      }
    })
  }

  /**
   * Add a task
   * @param task
   */
  add(task: Task): void {
    this.remove(task.id)
    if (!task.startDate) {
      this.backlog.value.set(task.id, task)
      return
    }

    if (
      moment(task.startDate).isAfter(maxDate.value) ||
      moment(task.estimatedEndMax).isBefore(minDate.value)
    ) {
      return
    }

    this.tasks.set(task.id, task)

    let workforceGroup = this.byWorkforce.value.get(task.assignedTo ?? null)
    if (!workforceGroup) {
      workforceGroup = new Map<Task['id'], Task>()
      this.byWorkforce.value.set(task.assignedTo, workforceGroup)
    }

    let businessCaseGroup = this.byBusinessCase.value.get(task.businessCase)
    if (!businessCaseGroup) {
      businessCaseGroup = new Map<Task['id'], Task>()
      this.byBusinessCase.value.set(task.businessCase, businessCaseGroup)
    }

    workforceGroup.set(task.id, task)
    businessCaseGroup.set(task.id, task)
  }

  /**
   * Removes a task
   */
  remove(id: Task['id']): void {
    this.backlog.value.delete(id)
    if (this.tasks.delete(id)) {
      // Deletion on submaps is O(n²)
      // Optimization: only delete on submaps if task was in this.tasks
      this.byBusinessCase.value.forEach((map) => map.delete(id))
      this.byWorkforce.value.forEach((map) => map.delete(id))
    }
  }

  /**
   * Get a specific task
   */
  getTask(id: Task['id']): Task | undefined {
    return this.tasks.get(id) || this.backlog.value.get(id)
  }

  /**
   * Patch a task (and persist it on backend)
   */
  patch(id: Task['id'], patch: Partial<Task>): boolean {
    const task = this.getTask(id)
    if (!task) return false
    Object.assign(task, patch)
    this.add(task)
    task.estimatedEndMax = moment(task.startDate)
      .clone()
      .add(task.durationMax, 'minutes')
      .toDate()
    taskClient.patch(id, patch).then((task) => this.add(task))
    return true
  }
}
