import { computed } from 'vue'
import type { i18n } from '@/types'
import moment, { Moment } from 'moment'
import { config, referenceDate, TaskTree } from '@/states/weekPlanning'
import { GetTasksParam, taskClient } from '@/api'
import { addMessage } from '@/states'

export const minDate = computed(() => {
  const date = moment(referenceDate.value).startOf('day')
  if (config.nbDays > 2) date.subtract(1, 'day')
  return date
})

export const maxDate = computed(() =>
  minDate.value
    .clone()
    .add(config.nbDays - 1, 'day')
    .endOf('day')
)

/**
 * Blocs of time for timeline
 */
export const blocks = computed<Moment[]>(() => {
  const blocks: Moment[] = []

  const date = minDate.value.clone()
  while (date.isBefore(maxDate.value)) {
    blocks.push(date.clone())
    date.add(config.hoursPerBlock, 'hour')
    if (blocks.length > 5000) {
      console.warn('Too many blocks generated, stopping at 5000')
      break
    }
  }
  return blocks
})

/**
 * List of days for the timeline
 */
export const days = computed(() => {
  const days: Moment[] = [blocks.value[0]]
  let day: Moment | null = blocks.value[0]
  for (const block of blocks.value as Moment[]) {
    if (!block.isSame(day, 'day')) {
      day = block
      days.push(block)
    }
  }
  return days
})

/**
 * Finds the blocks that belong to a weekend, and aggregates them into a single block to display it differently
 */
export const weekendBlocks = computed(() => {
  const weBlocks: Array<{
    startDate?: Date
    estimatedEndMax?: Date
  }> = []

  let stillWeekend = false
  for (const block of blocks.value as Moment[]) {
    if ([5, 6].includes(block.weekday())) {
      if (stillWeekend) {
        weBlocks[weBlocks.length - 1].estimatedEndMax = block.toDate()
      } else {
        weBlocks.push({
          startDate: block.toDate(),
          estimatedEndMax: block.toDate(),
        })
        stillWeekend = true
      }
    } else {
      if (stillWeekend) {
        weBlocks[weBlocks.length - 1].estimatedEndMax = block.toDate()
      }
      stillWeekend = false
    }
  }
  return weBlocks
})

/**
 * All groups of tasks
 */
export const taskTree = new TaskTree()

/**
 * Get all tasks of non-archived business cases
 */
export async function getAllVisibleTasks(t: i18n.t): Promise<void> {
  taskTree.clear()

  try {
    await taskTree.startLiveReload()
  } catch (e) {
    addMessage({
      summary: t('page.weekPlanning.failLiveReload.title'),
      detail: t('page.weekPlanning.failLiveReload.detail'),
      severity: 'warn',
      closable: true,
    })
  }

  const params: GetTasksParam = {
    page: 1,
    'period[from]': minDate.value.format(),
    'period[to]': maxDate.value.format(),
    archived: false,
  }

  if (config.selectedWorkforces.length) {
    params.assignedTo = config.selectedWorkforces
  }

  let result = await taskClient.getCollection(params)
  result.resources.forEach((task) => taskTree.add(task))
  while (result.next) {
    result = await result.next()
    result.resources.forEach((task) => taskTree.add(task))
  }
}
