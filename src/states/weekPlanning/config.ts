import { computed, reactive, watchEffect } from 'vue'
import { cloneDeep } from 'lodash'
import type { Workforce, PlanningSettingsModes } from '@/api'
import { i18n } from '@/types'
import { BY_WORK_FORCE } from '@/api'
import { settings } from '@/states'

export type Config = {
  row: PlanningSettingsModes

  /**
   * Anchor date of planning view, null date means center on relative today
   */
  date: Date | null

  hoursPerBlock: number
  nbDays: number
  blockWidth: number
  /**
   * List of workforces for which to display tasks
   */
  selectedWorkforces: Array<Workforce['id']>
}

export const DEFAULT_CONFIG: Config = {
  row: BY_WORK_FORCE,
  date: null,
  hoursPerBlock: 2,
  nbDays: 7,
  blockWidth: 40,
  selectedWorkforces: [],
}

export const config = reactive<Config>(cloneDeep(DEFAULT_CONFIG))

export const referenceDate = computed(() => config.date || new Date())

const baseI18n = 'page.weekPlanning.filters.granularityOptions'

/**
 * Hour per block possibilities
 */
export function getHoursPerBlockOptions(t: i18n.t = (str: string) => str) {
  return [
    { value: 0.25, label: t(`${baseI18n}.0_25`) },
    { value: 0.5, label: t(`${baseI18n}.0_5`) },
    { value: 1, label: t(`${baseI18n}.hour`, 1) },
    { value: 2, label: t(`${baseI18n}.hour`, 2) },
    { value: 3, label: t(`${baseI18n}.hour`, 3) },
    { value: 4, label: t(`${baseI18n}.hour`, 4) },
    { value: 6, label: t(`${baseI18n}.hour`, 6) },
    { value: 8, label: t(`${baseI18n}.hour`, 8) },
    { value: 12, label: t(`${baseI18n}.halfDay`) },
    { value: 24, label: t(`${baseI18n}.day`, 1) },
    { value: 48, label: t(`${baseI18n}.day`, 2) },
    { value: 168, label: t(`${baseI18n}.week`) },
  ]
}

watchEffect(async () => {
  if (settings.planningSettings) {
    config.row = settings.planningSettings.mode
    config.nbDays = settings.planningSettings.daysNumber
    config.hoursPerBlock = settings.planningSettings.granularity
    config.selectedWorkforces = settings.planningSettings.selectedWorkforces
    config.blockWidth = settings.planningSettings.blockWidth
  } else {
    Object.assign(config, cloneDeep(DEFAULT_CONFIG))
  }
})
