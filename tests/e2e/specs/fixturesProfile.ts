import { FixturesProfile } from '../utils/fixturesProfile'
import { getJwtsStub } from '../utils/jwtProvider'

export const fixturesProfile: FixturesProfile = {
  '/auth/authentication_token': getJwtsStub(),
  '/auth/refresh_token': getJwtsStub(),
  '/auth/users/85a830e4-1185-451f-a125-1af93ecb2b32': {
    fixture: 'api/users/user.json',
  },
}
