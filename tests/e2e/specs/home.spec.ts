import { fixturesProfile } from './fixturesProfile'
import { stubAll } from '../utils/stubAll'
import { guiLog, logIn, logOut } from '../utils/logging'

describe('Dashboard with one society access', () => {
  beforeEach(() => {
    cy.intercept(Cypress.env('API_ENDPOINT'), stubAll(fixturesProfile))
    logIn()
  })

  afterEach(() => {
    logOut()
  })

  it('Visit dashboard and sets society automatically', () => {
    logOut()
    guiLog('/#/home')
    cy.contains('h1', 'Home')
  })
})
