import { stubAll } from '../utils/stubAll'
import { guiLog, logOut } from '../utils/logging'

describe('Fail login', () => {
  afterEach(() => {
    logOut()
  })

  it('Log with wrong credentials', () => {
    cy.intercept(
      Cypress.env('API_ENDPOINT'),
      stubAll({
        '/auth/authentication_token': {
          statusCode: 401,
        },
      })
    )
    cy.visit('/')

    cy.get('.Login__Form .p-button').should('be.disabled')

    guiLog('/', false)

    cy.get('.Login__MessageError')
      .should('be.visible')
      .should(
        'have.text',
        'Provided credentials are invalid. Please, verify them.'
      )
  })

  it('Log and got server error', () => {
    cy.intercept(
      Cypress.env('API_ENDPOINT'),
      stubAll({
        '/auth/authentication_token': {
          statusCode: 500,
        },
      })
    )
    cy.visit('/')

    cy.get('.Login__Form .p-button').should('be.disabled')

    guiLog('/', false)

    cy.get('.Login__MessageError')
      .should('be.visible')
      .should(
        'have.text',
        'There is an error, verify your internet connection and contact support.'
      )
  })

  it('Use recovery flow', () => {
    cy.intercept(
      Cypress.env('API_ENDPOINT'),
      stubAll({
        '/auth/reset_password': {
          statusCode: 202,
        },
      })
    )
    cy.visit('/')

    cy.get('.Login__Form .p-button').should('be.disabled')

    // Going to recovery
    cy.get('.Login__Recovery').click()
    cy.get('.Login__Form .p-inputtext[type=password]').should('not.exist')

    // Testing back button
    cy.get('.Login__RecoveryBack').click()
    cy.get('.Login__Form .p-inputtext[type=password]').should('exist')

    // Recovery again
    cy.get('.Login__Recovery').click()
    cy.get('.Login__Form .Login__Submit')
      .should('be.disabled')
      .should('contain.text', 'Send my recovery link')

    cy.get('.Login__Form')
      .find('.p-inputtext[type=email]')
      .type('loth@orcanie.example')
    cy.get('.Login__Form .Login__Submit').should('be.enabled').click()

    // Back to log in with a message
    cy.get('.Login__Form .Login__Submit').should('contain.text', 'Log In')
    cy.get('.Login__MessageSuccess').should(
      'contain.text',
      'You should receive a mail with your recovery link soon.'
    )
    cy.get('.Login__MessageInfo').should(
      'contain.text',
      'If not, you may not have access to this service. Please check also your spam folder.'
    )
  })

  it('Use recovery link', () => {
    cy.intercept(
      Cypress.env('API_ENDPOINT'),
      stubAll({
        '/auth/reset_password/siLt1xHIsc9uMOJbjaNPapS1fn64ILxQSd8cQn8r': {
          statusCode: 200,
        },
      })
    )

    const randomToken = 'siLt1xHIsc9uMOJbjaNPapS1fn64ILxQSd8cQn8r'
    cy.visit(`/#/reset-password?token=${randomToken}`)

    cy.get('.Login__Token')
      .should('be.disabled')
      .should('have.value', randomToken)

    cy.get('.Login__Form .Login__Submit')
      .should('be.disabled')
      .should('contain.text', 'Change your password')

    cy.get('.Login__Form').find('.p-inputtext[type=password]').type('PortalNug')

    cy.get('.Login__Form .Login__Submit')
      .should('be.enabled')
      .scrollIntoView()
      .click()

    // Back to log in with a message
    cy.get('.Login__Form .Login__Submit').should('contain.text', 'Log In')
  })
})
