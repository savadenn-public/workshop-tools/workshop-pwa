import { StaticResponse } from 'cypress/types/net-stubbing'

/**
 * A specific fixture for a call
 */
export type StubCall = Partial<StaticResponse>

/**
 * A profile of fixture
 */
export type FixturesProfile = Record<string, StubCall>
