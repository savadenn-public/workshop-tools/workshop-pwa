import { StubCall } from './fixturesProfile'
import { encode } from 'jwt-simple'

export function getJwtsStub(): StubCall {
  return {
    body: {
      token: encode(
        {
          guid: '1',
        },
        'my-secret'
      ),
      refresh_token: 'my_super_secret_refresh',
    },
  }
}
