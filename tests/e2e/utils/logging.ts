import { Settings } from '../../../src/types/States'

export function guiLog(url: string, shouldLog = true): void {
  cy.visit(url)

  cy.get('.Login__Form', {})
    .find('.p-inputtext[type=email]')
    .type('rick@multiverse.example')

  cy.get('.Login__Form').find('.p-inputtext[type=password]').type('PortalGun')

  cy.get('.Login__Form .Login__Submit').should('be.enabled').click()

  if (shouldLog) {
    cy.get('.TopBar__Title').should('have.text', 'Workshop')
  } else {
    cy.get('.TopBar__Title').should('not.exist')
  }
}

export function logOut(): void {
  cy.clearLocalStorage()
}

const settings: Settings = {
  lang: 'en',
  theme: 'themeLight',
  timeZone: 'Europe/Paris',
  user: {
    id: '/auth/users/85a830e4-1185-451f-a125-1af93ecb2b32',
    personalData: {
      name: 'Rick Sanchez',
      email: 'rick@multiverse.example',
    },
  },
  refreshToken: 'my_super_secret_refresh',
}

export function logIn(): void {
  window.localStorage.setItem('settings', JSON.stringify(settings))
}
